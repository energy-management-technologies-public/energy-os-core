from typing import Callable
import time
import threading
import queue
from multiprocessing import AuthenticationError
from multiprocessing.connection import Listener, Client, deliver_challenge
from eos.core.common import APIRequest
from eos.core.request import Request
from eos.core.resource_pointer import ResourcePointer, RPManager, Permission
import os
import platform
import argparse
import traceback
import datetime

class Server(RPManager):
    """
    A Server for EOS.

    Parameters
    ----------
    eos_server_dir_absolute : str, default=''
        The absolute path to the directory to be used as the base for the EOS
        Server and its data.

    Methods
    -------
    handleUpdates(rp_name, updates)
    start(callback)

    """

    IS_WINDOWS = platform.system() == 'Windows'
    EOS_SERVER_DIR = 'eos_server'
    # 'AF_UNIX' for between processes on same host on Linux, AF_PIPE on Windows.
    INTERNAL_ADDRESS = 'internal'
    EXTERNAL_ADDRESS = ('localhost', 6001) # 'AF_INET' for TCP/IP
    RP_SUBDIR = 'resources'
    AUTHKEYS_FILE = 'authkeys.txt' # For local apps with this server.
    SYMKEYS_FILE = 'symkeys.txt' # Symmetric keys between two servers.
    MAX_DEPTH = 100 # Longest possible link chain for resource pointer

    @staticmethod
    def GetInternalServerAddr(base) -> str:
        if Server.IS_WINDOWS:
            return os.path.join(r'\\.\pipe',
                                base.split(':')[-1], # Remove the Windows drive.
                                Server.INTERNAL_ADDRESS)
        else:
            return os.path.join(base, Server.INTERNAL_ADDRESS)

    def __init__(self, eos_server_dir_absolute='', external_addr=()):
        self._setup_state(external_addr)
        self._setup_files_and_resource(eos_server_dir_absolute)
        self._load_keys()

    def _setup_state(self, external_addr):
        self._app_authkeys = {}
        self._server_symkeys = {}
        self._current_RPs = {}
        self._connections = {}
        self._remote_hosts = {}
        self._external_addr = external_addr or self.EXTERNAL_ADDRESS

    def _setup_files_and_resource(self, eos_server_dir_absolute):
        # Create eos folder if does not already exist.
        self._eos_server_dir = eos_server_dir_absolute
        if not eos_server_dir_absolute:
            self._eos_server_dir = os.path.join(os.getcwd(),
                                                self.EOS_SERVER_DIR)
        if not os.path.exists(self._eos_server_dir):
            os.mkdir(self._eos_server_dir)
        
        # Create resources subfolder if does not already exist.
        self._resources_dir = os.path.join(self._eos_server_dir,
                                           self.RP_SUBDIR)
        if not os.path.exists(self._resources_dir):
            os.mkdir(self._resources_dir)
        ResourcePointer.SetRootDir(self._resources_dir)
        ResourcePointer.SetManager(self)
        
        # Create authkey file path.
        self._authkeys_file_abs = os.path.join(self._eos_server_dir,
                                          self.AUTHKEYS_FILE)
        if not os.path.exists(self._authkeys_file_abs):
            with open(self._authkeys_file_abs, 'a') as f:
                # Creates the file if it does not yet exist.
                pass

        # Create symkey file path.
        self._symkeys_file_abs = os.path.join(self._eos_server_dir,
                                          self.SYMKEYS_FILE)
        if not os.path.exists(self._symkeys_file_abs):
            with open(self._symkeys_file_abs, 'a') as f:
                # Creates the file if it does not yet exist.
                pass
        return

    def _load_keys(self):
        with open(self._authkeys_file_abs, 'r') as f:
            for line in f.readlines():
                id, authkey = line.strip().split(':')
                # This is only building the server, before processes are forked.
                self._app_authkeys[int(id)] = bytes(authkey, 'utf-8')
        with open(self._symkeys_file_abs, 'r') as f:
            for line in f.readlines():
                host, port, symkey = line.strip().split(':')
                # This is only building the server, before processes are forked.
                self._server_symkeys[host] = (int(port), bytes(symkey, 'utf-8'))
    
    def _get_stored_rp(self, rp_name):
        # TODO: Add Lock when multithreading.
        rp = None
        if rp_name in self._current_RPs:
            rp = self._current_RPs[rp_name]
        # Try to load from file if not in current list
        else:
            rp = ResourcePointer.LoadResearchPointer(rp_name)
            if rp:
                # Add this to seen RPs for faster access in the future.
                self._current_RPs[rp_name] = rp
        return rp
    
    def _get_rp_for_request(self, rp_name, request_type: APIRequest):
        rp = self._get_stored_rp(rp_name)
        if rp and rp.is_link() and (request_type == APIRequest.SUBSCRIBE or
                                    request_type == APIRequest.READ or
                                    request_type == APIRequest.WRITE):
            return self._get_rp_for_request(rp.get_ref(), request_type)
        return rp
    
    def _remove_rp(self, rp_name):
        # TODO: Add Lock when multithreading.
        if rp_name in self._current_RPs:
            del self._current_RPs[rp_name]
        return

    # Returns (has_access:bool, remote_name:str)
    # If is_remote is not None, will need to send a remote request to verify
    # access.
    def _has_access(self,
                app_id,
                request_type:APIRequest,
                rp_name:str,
                depth:int):
        if depth > self.MAX_DEPTH:
            return False, None

        names = rp_name.split(':')
        if len(names) > 1:
            if len(names) != 2:
                return False, None
            if names[0] == self._external_addr[0]:
                rp_name = names[1]
            else:
                return False, names
        
        rp = self._get_stored_rp(rp_name)
        
        permission_type = Permission.NONE
        if request_type == APIRequest.CREATE:
            # If it is create, this has access if the ResourcePointer does not
            # yet exist, so can exist early.
            return rp == None, None
        elif request_type == APIRequest.DESTROY:
            if rp == None:
                return False, None
            permission_type = Permission.OWNER
        elif request_type == APIRequest.COPY:
            permission_type = Permission.READ
        elif request_type == APIRequest.CHOWN:
            permission_type = Permission.OWNER
        elif request_type == APIRequest.SUBSCRIBE:
            permission_type = Permission.READ
        elif request_type == APIRequest.READ:
            permission_type = Permission.READ
        elif request_type == APIRequest.WRITE:
            permission_type = Permission.WRITE
        elif request_type == APIRequest.GET_CONFIG:
            permission_type = Permission.OWNER
        else:
            assert False, 'Not reachable: ' + str(request_type)

        if (rp == None or
            rp.check_access_shallow(app_id, permission_type) == False):
            return False, None
        
        # For certain request types, we do not only want to check the current
        # ResourcePointer, but also the chain. This is the supplemental check.
        if rp.is_link() and (request_type == APIRequest.SUBSCRIBE or
                             request_type == APIRequest.READ or
                             request_type == APIRequest.WRITE):
            return self._has_access(app_id, request_type, rp.get_ref(), depth+1)
        else:
            return True, None

    def _handle_request_remote(self, conn, app_id, remote_host, request_type, rp_name, args):
        port, symkey = self._server_symkeys.get(remote_host, (None, None))
        if not symkey:
            conn.send((False, None))
            return
        addr = (remote_host, port)
        id = self._external_addr[0]
        Request.Setup(addr, id, symkey)
        request = Request(request_type, rp_name, args, app_id)
        success, data = request.send(local=False)
        conn.send((success, data))
        return


    def _handle_request(self, conn, last_accepted):
        # Figure out who request is from / authenticate
        local, id = conn.recv()
        key = None
        if local:
            key = self._app_authkeys.get(id)
        else:
            _, key = self._server_symkeys.get(id, (None, None))
        if not key:
            conn.send((False, None))
            return
        try:
            deliver_challenge(conn, key)
        except AuthenticationError:
            print('Illegal access from:',
                last_accepted,
                'posing as',
                id)
            return

        # Find out what the request wants.
        calling_app_id, request_int, rp_name, args = conn.recv()

        # Only use calling app_id as app_id if we get the call from a server.
        # We only trust servers, not individual apps.
        # TODO: Enforce servers to only be able to send calling_app_id of apps
        # in their local EnergyOS network.
        if not local:
            app_id = calling_app_id
        else:
            app_id = id

        request_type = APIRequest(request_int)
        if request_type == APIRequest.PING:
            conn.send((True, None))
            return
        
        if request_type == APIRequest.REGISTER_CHANNEL:
            addr = args[0]
            self._connections[app_id] = addr
            print('\n{} -- Received new connection from app: {}'\
                  .format(datetime.datetime.now(), app_id))
            conn.send((True, None))
            return

        if not local and request_type == APIRequest.SEND_UPDATE:
            full_rp_name = rp_name
            updates = [(app_id, args[0])]
            self._handle_updates_internal(full_rp_name, updates)
            conn.send((True, None))
            return

        if type(rp_name) != str:
            conn.send((False, None))
            return

        access, remote_name = self._has_access(app_id, request_type, rp_name, 1)
        if remote_name:
            self._handle_request_remote(conn,
                                        app_id,
                                        remote_name[0],
                                        request_type,
                                        remote_name[1],
                                        args)
            return

        if not access:
            conn.send((False, None))
            return
        
        names = rp_name.split(':')
        if len(names) > 1:
            # Should be for sure a length 2 array because of self._has_access
            # check prior.
            rp_name = names[1]

        rp = self._get_rp_for_request(rp_name, request_type)
        result = None
        try:
            if request_type == APIRequest.CREATE:
                assert rp == None, 'Resource Pointer should not exist.'
                result = ResourcePointer.Create(name=rp_name,
                                                cycle_s=args[0],
                                                owner=app_id)
                if result == None:
                    conn.send((False, None))
                    return
            elif request_type == APIRequest.DESTROY:
                rp.destroy()
                self._remove_rp(rp_name)
            elif request_type == APIRequest.COPY:
                rp.copy(copy_name=args[0], copy_owner=app_id, is_link=args[1])
            elif request_type == APIRequest.CHOWN:
                rp.chown(app_id=args[0], permission=args[1], priority=args[2])
            elif request_type == APIRequest.SUBSCRIBE:
                rp.subscribe(app_id=app_id, type=args[0])
                if not local:
                    self._remote_hosts[app_id] = id
                result = ':'.join((str(self._external_addr[0]), rp_name))
            elif request_type == APIRequest.READ:
                result = rp.read(start_time=args[0], end_time=args[1])
            elif request_type == APIRequest.WRITE:
                success = rp.write(entries=args[0])
                if not success:
                    err_msg = ('Could not write any entries\n'
                               'name: {name}\n'
                               'args: {args}\n').format(name=rp_name,
                                                        args=args)
                    raise Exception(err_msg)
            elif request_type == APIRequest.GET_CONFIG:
                result = rp.get_config()
            else:
                assert False, 'Not reachable'
        except Exception:
            print(traceback.format_exc())
            print('Resuming...')
            conn.send((False, None))
            return

        conn.send((True, result))
        return
    
    def _server_task(self, name, address, family, task_queue,
                     callback:Callable=None):
        with Listener(address=address,
                      family=family,
                      backlog=16) as listener:
            print('\n({name}) EOS Server listening at {addr}'\
                  .format(name=name, addr=address if family != 'AF_INET' \
                          else ':'.join(map(str, address))))
            if callback:
                callback()
            while True:
                conn = listener.accept()
                task = (conn, listener.last_accepted)
                task_queue.put(task)

    def _send_remote_update(self,
                            host:str,
                            app_id:int,
                            full_rp_name:str,
                            timestamps:list[int]):
        port, symkey = self._server_symkeys.get(host, (None, None))
        if not symkey:
            assert False, 'Should have a shared key with this host.'
        addr = (host, port)
        id = self._external_addr[0]
        Request.Setup(addr, id, symkey)
        request = Request(APIRequest.SEND_UPDATE,
                          full_rp_name,
                          args=[timestamps],
                          app_id=app_id)
        # Best effort sending. If does not work, we print the error.
        try:
            success, _ = request.send(local=False)
        except Exception as e:
            success = False
        
        if not success:
            print('Could not update app', app_id, 'about', full_rp_name)

    def _handle_updates_internal(self,
                                 full_rp_name:str,
                                 updates:list[(int, list[int])]):
        """
        Handles an updates to the Resource Pointer full_rp_name.

        Parameters
        ----------
        full_rp_name : str
            The name of the resource that was updated. Host and rp_name.
        updates: list[(int, int, int)]
            A list of (app_id, [start_times]) objects to be reported.
        """
        for app_id, timestamps in updates:
            # Send the remote update if necessary.
            remote_host = self._remote_hosts.get(app_id)
            if remote_host:
                self._send_remote_update(remote_host,
                                         app_id,
                                         full_rp_name,
                                         timestamps)
                continue

            # Handle local updates.
            addr = self._connections.get(app_id)
            try:
                conn = Client(address=addr, family='AF_INET')
                # TODO: Add auth check for update.
                conn.send((full_rp_name, timestamps))
                conn.close()
            except Exception as e:
                print('Failed to send update to app_id', app_id)
                print(traceback.format_exc())
        return


    # overriding abstract method
    def handleUpdates(self, rp_name:str, updates:list[(int, list[int])]):
        """
        Handles an updates to the Resource Pointer rp_name. This is called by a
        Resource Pointer after its data has been changed.

        Parameters
        ----------
        rp_name : str
            The name of the resource that was updated.
        updates: list[(int, int, int)]
            A list of (app_id, [start_times]) objects to be reported.
        """
        self._handle_updates_internal(self._external_addr[0] + ':' + rp_name,
                                      updates)
    
    def start(self, callback:Callable=None):
        """
        Starts the Server.

        Parameters
        ----------
        callback : function()
            The function to be called after the server is turned on. This
            function must return quickly, otherwise it blocks connections to
            the server.
        """
        # Create task queue.
        task_queue = queue.Queue()

        # Create internal server thread.
        internal_addr = self.GetInternalServerAddr(self._eos_server_dir)
        if self.IS_WINDOWS:
            threading.Thread(target=self._server_task,
                            args=('internal',
                                internal_addr,
                                'AF_PIPE',
                                task_queue,
                                callback)).start()
        else:
            if os.path.exists(internal_addr):
                os.remove(internal_addr)
            threading.Thread(target=self._server_task,
                            args=('internal',
                                internal_addr,
                                'AF_UNIX',
                                task_queue,
                                callback)).start()
        
        # Create external server thread.
        threading.Thread(target=self._server_task,
                         args=('external',
                               self._external_addr,
                               'AF_INET',
                               task_queue,
                               None)).start()

        # Worker / task handing in this thread.
        # TODO: Spin up new threads or processes to parallelize this more in the
        # future. Huge multithreading undertaking to get right.
        while True:
            if task_queue.empty():
                # If secs is zero, the thread relinquishes the remainder of its
                # time slice to any other thread that is ready to run. If there
                # are no other threads ready to run, the function returns
                # immediately, and the thread continues execution.
                time.sleep(0)
            else:
                try:
                    task = task_queue.get_nowait()
                    conn, last_accepted = task
                    self._handle_request(conn, last_accepted)
                    conn.close()
                except queue.Empty as e:
                    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--dir",
        type=str,
        required=False,
        help='The relative path to the directory to be used as the base for ' \
            'the EOS Server and its data.',
    )
    parser.add_argument(
        "-a",
        "--host_addr",
        type=str,
        required=False,
        default=Server.EXTERNAL_ADDRESS[0],
        help='The host to listen for external EOS connections. Default is ' + \
            str(Server.EXTERNAL_ADDRESS[0]),
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        required=False,
        default=Server.EXTERNAL_ADDRESS[1],
        help='The port to listen for external EOS connections. Default is ' + \
            str(Server.EXTERNAL_ADDRESS[1]),
    )
    args = parser.parse_args()
    server_dir = os.path.join(os.getcwd(), args.dir or Server.EOS_SERVER_DIR)
    # TODO: Check if running with high privileges (so that not just any user
    # can run it.. should be a system service).
    s = Server(server_dir, (args.host_addr, args.port)).start()

