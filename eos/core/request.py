from multiprocessing import AuthenticationError
from multiprocessing.connection import Client, answer_challenge
from eos.core.common import APIRequest
import platform

class Request:
    IS_WINDOWS = platform.system() == 'Windows'
    SERVER_REQUEST_ADDR:str = ""
    ID:int = -1 # Can be a string if used for a server.
    AUTHKEY:bytes = b''

    @staticmethod
    def Setup(server_addr, id, authkey):
        Request.SERVER_REQUEST_ADDR = server_addr
        Request.ID = id
        Request.AUTHKEY = authkey
        return
    
    def __init__(self, type: APIRequest, name="", args=[], app_id=-1):
        self._type = type
        self._args = args
        self._name = self._resolve_name_if_possible(name)
        # Only used in a remote call, i.e. between servers.
        self._app_id = app_id
    
    def _resolve_name_if_possible(self, name):
        # TODO: Resolve name properly.
        # Could be create call, so fallback is name.
        return name

    def _get_addr_family(self, local):
        if local:
            return 'AF_PIPE' if self.IS_WINDOWS else 'AF_UNIX'
        return 'AF_INET'

    def send(self, local=True):
        # TODO: Maybe add threading?
        with Client(address=self.SERVER_REQUEST_ADDR,
                    family=self._get_addr_family(local)) as conn:
            conn.send((local, self.ID))
            try:
                answer_challenge(conn, self.AUTHKEY)
            except AuthenticationError:
                print("Request rejected: ", self._type, self._name)
                return False, None
            conn.send([self._app_id,
                       self._type.value,
                       self._name,
                       self._args])
            return conn.recv()