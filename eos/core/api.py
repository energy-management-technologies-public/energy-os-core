import os
import errno
import json
from typing import Callable
from eos.core.request import Request
from eos.core.common import (Permission,
                             Subscription,
                             Priority,
                             APIRequest,
                             IOObject)
from eos.core.subscription_handler import SubscriptionHandler
import threading
import platform

class API():
    """
    The API class for interacting with an EOS Server.

    Parameters
    ----------
    auth_config_json : str
        The absolute path to the json file containing the authorization details
        of the calling app. This should include server_addr, app_id and authkey.

    Methods
    -------
    Ping()
    Create(name, cycle_s)
    Destroy(name)
    Copy(name, copy_name, is_link)
    Chown(name, app_id, permission, priority)
    Subscribe(name, type, handler)
    Read(name, start_time_s, end_time_s)
    Write(name, entries)
    GetConfig(name)

    """

    def __init__(self, auth_config_json):
        self._subscription_handler = SubscriptionHandler()
        self._connect_to_server(auth_config_json)

    def _connect_to_server(self, auth_config_json):
        # Pull server_addr, app_id and authkey from auth file.
        try:
            with open(auth_config_json) as f:
                auth_obj = json.load(f)
        except:
            raise Exception('Need to pass auth file json to API.')
        
        Request.Setup(auth_obj['server_addr'],
                      auth_obj['app_id'],
                      bytes(auth_obj['authkey'], 'utf-8'))
        
        # Check if surver is on, using Ping API call.
        success = self.Ping()
        if not success:
            raise Exception('The connection to the server was unsuccessful. '
                            'Either:\n'
                            '(1) You have the wrong server address.\n'
                            '(2) The server is down.\n'
                            '(3) You need to register your app.\n'
                            '(4) You did not provide a valid auth file json.')
        
        # Create Pipe that will be used for subscriptions.
        addr = self._subscription_handler.start_and_get_addr()
        failed = addr == None or not self._register_for_updates(addr)
        if failed:
            raise Exception('Could not register update channel with server.')
        return
    
    def _register_for_updates(self, addr:tuple[str, int]):
        request = Request(APIRequest.REGISTER_CHANNEL, args=[addr])
        success, _ = request.send()
        return success

    
    def Ping(self) -> bool:
        """
        Pings the EOS Server to see if connection to server is working.

        Returns
        -------
        bool
            Whether this operation was successful or not. The server could be
            down or this app is not authorized.
        """
        request = Request(APIRequest.PING)
        try:
            success, _ = request.send()
        except Exception as e:
            return False
        return success


    def Create(self, name:str, cycle_s:int) -> bool:
        """
        Creates a Resource Pointer for the resource with a given name and cycle.

        Parameters
        ----------
        name : str
            The desired name of the resource. Valid characters are in
            ResourcePointer.ALLOWED_CHARACTERS.
        cycle_s: int
            The primary update cycle of the resource, in seconds. This
            determines the period of time that written values are valid
            for. With a particular cycle period, there can only be one valid at
            a time.

        Returns
        -------
        bool
            Whether this operation was successful or not. For example, this can
            fail if this resource already exists.
        """
        request = Request(APIRequest.CREATE, name, [cycle_s])
        success, _ = request.send()
        return success

    def Destroy(self, name:str) -> bool:
        """
        Destroys the Resource Pointer for the resource with a given name, if
        any.

        Parameters
        ----------
        name : str
            The desired name of the resource.
        
        Returns
        -------
        bool
            Whether this operation was successful or not. For example, this can
            fail if this resource does not exist or you do not have permission.
        """
        request = Request(APIRequest.DESTROY, name)
        success, _ = request.send()
        return success

    def Copy(self, name:str, copy_name:str, is_link:bool=False) -> bool:
        """
        Copies the Resource Pointer name and creates a copy named copy_name.

        Parameters
        ----------
        name : str
            The name of the resource to be copied.
        copy_name : str
            The name of the copy.
        link : bool, default=True
            Whether this copy should be a link, meaning it just acts as an alias
            to the original resource pointer, or if the contents should be
            copied into a new copy with new ownership and access rights.
        
        Returns
        -------
        bool
            Whether this operation was successful or not. For example, this can
            fail if this resource does not exist or you do not have permission.
        """
        request = Request(APIRequest.COPY, name, [copy_name, is_link])
        success, _ = request.send()
        return success

    def Chown(self,
              name:str,
              app_id:int,
              permission:Permission=None,
              priority:Priority=None) -> bool:
        """
        Changes the permissions and priority of the app_id's access to the
        Resource Pointer named name.

        Parameters
        ----------
        name : str
            The name of the resource to be changed.
        app_id: int
            The app id of the app whose permissions/priority will be changed.
        permission: Permission, default=None
            The permission to be set for this app. If None, keep the permission
            that is already set, or use the default if the app is new. The
            default is Permission.NONE.
        priority: Priority, default=None
            The priority to be set for this app. If None, keep the priority that
            is already set, or use the default if the app is new. The default is
            Priority.LOW.
        
        Returns
        -------
        bool
            Whether this operation was successful or not. For example, this can
            fail if this resource does not exist or you do not have permission.
        """
        request = Request(APIRequest.CHOWN, name, [app_id, permission, priority])
        success, _ = request.send()
        return success
        
    def Subscribe(self,
              name:str,
              type:Subscription,
              handler:Callable[[str, list[int]], None]) -> bool:
        """
        Subcribes to the Resource Pointer name, with a particular Subscription
        type and a handler function for updates. Note: Subscriptions do not
        survive server restart. Apps will need to re-subscribe.

        Parameters
        ----------
        name : str
            The name of the resource to be subscribed to.
        type: Subcription
            The type of subscription this is. This indicates when updates are
            sent for changes to this resource.
        handler: function(str, [int])
            The handler function that should be run when an update to this
            resource occurs. The function will be passed the resource name and
            a list of start times that were changed.
        
        Returns
        -------
        bool
            Whether this operation was successful or not. For example, this can
            fail if this resource does not exist or you do not have permission.
        """
        request = Request(APIRequest.SUBSCRIBE, name, [type])
        success, rp_name = request.send()

        if success:
            self._subscription_handler.register_handler(rp_name,
                                                        type,
                                                        handler)
        return success

    def Read(self,
              name:str,
              start_time_s:int=None,
              end_time_s:int=None) -> (bool, IOObject):
        """
        Reads values from cycles of the Resource Pointer name that are within
        the range of start_time (inclusive) and end_time (exclusive).

        Parameters
        ----------
        name : str
            The name of the resource to be subscribed to.
        start_time_s: int
            The lower bound (inclusive) of the cycles to be read, in seconds.
        end_time_s: int
            The lower bound (inclusive) of the cycles to be read, in seconds.
        
        Returns
        -------
        bool
            Whether this operation was successful or not. For example, this can
            fail if this resource does not exist or you do not have permission.
        entries: IOObject
            If successful, this a list of (start_time, value) tuples of the
            valid cycles within the given range. Times are aligned to the
            beginning of the cycles.
        """
        request = Request(APIRequest.READ, name, [start_time_s, end_time_s])
        success, data = request.send()
        return success, data

    def Write(self,
              name:str,
              entries:IOObject) -> bool:
        """
        Writes new values to the Resource Pointer name. Unaligned timestamps are
        automatically aligned to the start of valid cycles and if two writes
        effect the same cycle, the second one is skipped. Writes to cycles
        whose end times are in the past are skipped.

        Parameters
        ----------
        name : str
            The name of the resource to be subscribed to.
        entries: IOObject
            The IOObject containing the list of (timestamp, value) pairs to be
            written.
        
        Returns
        -------
        bool
            Whether this operation was successful or not. For example, this can
            fail if this resource does not exist or you do not have permission.
        """
        request = Request(APIRequest.WRITE, name, [entries])
        success, _ = request.send()
        return success
    
    def GetConfig(self, name:str) -> (bool, dict):
        """
        Gets the configuration dict for a particular resource pointer, including
        its name, cycle, access list, and whether it is a link or not.

        Parameters
        ----------
        name : str
            The name of the resource.
        
        Returns
        -------
        config: dict
            A dictionary for the configuration of the resource.
            {
                'name': str,
                'cycle_s': int,
                'access_list': {
                    'id': int,
                    'permission': Permission,
                    'priority': Priority,
                    'subscription': Subscription
                },
                'is_link': bool
            }
        """
        request = Request(APIRequest.GET_CONFIG, name)
        success, config = request.send()
        return success, config