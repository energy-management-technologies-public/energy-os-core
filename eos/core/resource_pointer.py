from __future__ import annotations
import jsonpickle
import datetime as dt
import os
import string
from abc import ABC, abstractmethod
from eos.core.common import (Permission,
                             Subscription,
                             Priority,
                             IOObject)

class RPManager(ABC):
    @abstractmethod
    def handleUpdates(self, rp_name:str, updates:list[(int, list[int])]):
        """
        Handles updates to the Resource Pointer rp_name. This is called by a
        Resource Pointer after its data has been changed.

        Parameters
        ----------
        rp_name : str
            The name of the resource that was updated.
        updates: list[Tuple[int, List[int]]]
            A list of (app_id, [start_times]) objects to be reported.
        """
        pass

class ResourcePointer:
    ROOT_DIR = ''
    MANAGER = None
    ALLOWED_CHARACTERS = set(string.ascii_lowercase + string.digits + '._-')

    @staticmethod
    def SetRootDir(root_dir) -> None:
        ResourcePointer.ROOT_DIR = root_dir
        return
    
    @staticmethod
    def SetManager(manager: RPManager) -> None:
        ResourcePointer.MANAGER = manager
        return
    
    @staticmethod
    def GetManager() -> RPManager:
        return ResourcePointer.MANAGER

    @staticmethod
    def Create(name, cycle_s, owner) -> bool:
        def check(test_str):
            return set(test_str) <= ResourcePointer.ALLOWED_CHARACTERS
        if not check(name):
            return None
        rp = ResourcePointer(name, cycle_s)
        rp._create(owner)
        return rp
    
    @staticmethod
    def GetNowSeconds() -> float:
        return dt.datetime.now().timestamp()
    
    @staticmethod
    def GetFileName(name) -> str:
        return os.path.join(ResourcePointer.ROOT_DIR, name + '.json')
    
    @staticmethod
    def LoadResearchPointer(name) -> ResourcePointer:
        if os.path.exists(ResourcePointer.GetFileName(name)):
            with open(ResourcePointer.GetFileName(name)) as f:
                json_str = f.read()
                rp = jsonpickle.decode(json_str, keys=True)
                return rp
        return None

    def __init__(self, name: str, cycle_s: int):
        self._name = name
        self._cycle_s = cycle_s
        self._access_list: {int: (Permission, Priority, Subscription)} = None
        self._data: {int:object} = {}
        self._ref = None
        self._oldest_w = -1
    
    def _save(self):
        with open(self._get_file_name(), 'w', encoding='utf-8') as f:
            json_obj = jsonpickle.encode(self, keys=True)
            f.write(json_obj)
        return
    
    def _assert_init(self):
        assert bool(self._access_list) == False
        assert bool(self._data) == False
        assert bool(self._ref) == False
        return
    
    def _add_owner(self, owner_id):
        self._access_list = {owner_id: (Permission.OWNER.value,
                                         Priority.MEDIUM.value,
                                         Subscription.NONE.value)}
        return

    def check_access_shallow(self, app_id, req_type: Permission):
        def allowed(app_permission_int):
            if (app_permission_int == Permission.OWNER.value):
                return True
            elif (app_permission_int == Permission.READ.value):
                return req_type == Permission.READ
            elif (app_permission_int == Permission.WRITE.value):
                return req_type == Permission.WRITE
            elif (app_permission_int == Permission.READ_WRITE.value):
                return req_type == Permission.READ or req_type == Permission.WRITE
            else:
                assert False, ('There is no scenario where this should be called '
                            'to check for the NONE permission.')
        # TODO: Add access for public resources after making app_id's strings.
        # if '*' in self._access_list:
        #     # If everyone can access this, return yes.
        #     global_permission_int,_,_ = self._access_list['*']
        #     if allowed(global_permission_int):
        #         return True
        app = self._access_list.get(app_id)
        if not app:
            return False
        app_permission_int,_,_ = app
        return allowed(app_permission_int)

    
    def _get_file_name(self):
        return ResourcePointer.GetFileName(self._name)

    def _create(self, owner_id):
        if os.path.exists(self._get_file_name()):
            raise FileExistsError('Cannot create resource that already exists')
        
        self._assert_init()
        self._add_owner(owner_id)
        self._ref = None
        self._save()
        return
    
    def _copy(self, copy_owner, ref, is_link: bool, data: dict):
        self._assert_init()
        self._add_owner(copy_owner)
        if is_link:
            self._ref = ref
            self._data = None
        else:
            self._ref = None
            self._data = data
        self._save()
        return
    
    def _align_timestamps(self, timestamps):
        assert self._oldest_w != -1
        aligned = []
        for t in timestamps:
            delta = (t - self._oldest_w ) % self._cycle_s
            aligned.append(t - delta)
        return aligned
    
    def _new_data_available(self, current_cycle_start, timestamps_written):
        if len(timestamps_written) == 0:
            return
        next_cycle_start = current_cycle_start + self._cycle_s

        has_current_update = current_cycle_start in timestamps_written
        has_next_update = next_cycle_start in timestamps_written
        updates:list[(int, list[int])] = []
        for app_id, [_,_, sub] in self._access_list.items():
            if sub == Subscription.NONE.value:
                continue
            elif sub == Subscription.ALL.value:
                updates.append((app_id, timestamps_written))
            elif sub == Subscription.CURRENT.value and has_current_update:
                updates.append((app_id, [current_cycle_start]))
            elif sub == Subscription.NEXT.value and has_next_update:
                updates.append((app_id, [next_cycle_start]))

        if len(updates) > 0:
            self.GetManager().handleUpdates(self._name, updates)
        return
    
    def is_link(self):
        return self._ref is not None
    
    def get_ref(self):
        return self._ref
    
    def destroy(self):
        # TODO: Do cleanup
        filename = self._get_file_name()
        if os.path.exists(filename):
            os.remove(filename)
        return
    
    def chown(self,
              app_id: str,
              permission: Permission=None,
              priority: Priority=None):
        assert permission != Permission.OWNER
        app = self._access_list.get(app_id)
        if not app:
            # Set defaults
            app = (Permission.NONE.value,
                   Priority.LOW.value,
                   Subscription.NONE.value)
        if permission:
            app = (permission.value, app[1], app[2])
        if priority:
            app = (app[0], priority.value, app[2])
        
        self._access_list[app_id] = app
        self._save()
        return

    def copy(self, copy_name, copy_owner, is_link) -> ResourcePointer:
        rp = ResourcePointer(copy_name, self._cycle_s)
        rp._copy(copy_owner, self._name, is_link, self._data.copy())
        return rp
    
    def subscribe(self, app_id, type: Subscription):
        if self.is_link():
            return
        app = self._access_list.get(app_id)
        assert app, 'App is not part of Resource Pointer access list'
        self._access_list[app_id] = (app[0], app[1], type.value)
        self._save()
        return
    
    
    def read(self, start_time=None, end_time=None) -> IOObject:
        if self.is_link():
            raise NotImplementedError('Tried to access a link RP. '
                                      'Should instead access its reference RP.')
        start_time = start_time or int(ResourcePointer.GetNowSeconds())
        # To ensure in same cycle. Otherwise could give two.
        end_time = end_time or start_time + 1
        if end_time <= start_time or not self._data:
            return []
        
        # Want cycles that fall into the request range to be returned.
        # That means that even if the requested end_time - start_time == cycle,
        # we can return two values, if the request does not align.
        # To do this, we keep end_time static and align start_time.
        [start_time] = self._align_timestamps([start_time])

        data = []
        collect = False
        # The persisted value that is still valid for start_time, if any.
        persisted_v = None
        for t, v in sorted(self._data.items()):
            if t >= start_time:
                collect = True
            if not collect:
                persisted_v = v
                continue
            if t >= end_time:
                break
            data.append((t, v))
        
        if persisted_v is not None and (len(data) == 0 or
                                        data[0][0] != start_time):
            data = [(start_time, persisted_v)] + data

        return data

    def write(self, entries:IOObject):
        if self.is_link():
            return False
        if not entries:
            return False
        entries.sort()
        timestamps, values = zip(*entries)
        # Assert entries are unique.
        assert len(timestamps) == len(set(timestamps)), timestamps

        first_write = False
        if self._oldest_w == -1:
            first_write = True
            self._oldest_w = timestamps[0]
        
        timestamps = self._align_timestamps(timestamps)

        [current_cycle_start] = self._align_timestamps(
                                    [int(ResourcePointer.GetNowSeconds())])

        old = -1
        timestamps_written = []
        for t,v in zip(timestamps, values):
            # Only write one value if multiple are attempting to be inputted for
            # same cycle. Could alternatively error.
            if t == old:
                continue
            old = t
            # Only write if t is in current cycle or later. (Can add a little
            # delay for current cycle start if the timing starts to become an
            # issue because of transmission time).
            # If this is the first write, it is always valid.
            if t >= current_cycle_start or first_write:
                first_write = False
                self._data[t] = v
                timestamps_written.append(t)
        self._save()
        self._new_data_available(current_cycle_start, timestamps_written)
        return len(timestamps_written) > 0
    
    def get_config(self):
        access_list_readable = []
        for id, app in self._access_list.items():
            per, prio, sub = app
            access_list_readable.append({
                "id": id,
                "permission": Permission(per).name,
                "priority": Priority(prio).name,
                "subscription": Subscription(sub).name
            })
        return { 
            'name': self._name,
            'cycle_s': self._cycle_s,
            'access_list': access_list_readable,
            'is_link': self.is_link()
        }


        
        
    
        
    