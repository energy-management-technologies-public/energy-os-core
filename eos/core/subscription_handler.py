import threading
import queue
from multiprocessing.connection import Listener
from typing import Callable
from eos.core.common import Subscription

class SubscriptionHandler:
    POLLING_RATE_S = 1
    MAX_UPDATES = 10

    def __init__(self):
        # TODO: Maybe make a internal socket version for Unix and Windows using
        # AF_UNIX and AF_PIPE.
        # Automatically select an available port
        self._listener_address = ('localhost', 0)
        self._update_queue = queue.Queue()
        self._listener_thread = None
        self._handlers: dict[str, Callable[[str, list[int]], None]] = {}
        self._polling = False

    def _update_listener(self, address, update_queue):
        with Listener(address=address,
                      family='AF_INET') as listener:
            # Send back address as the first message of the queue.
            addr = listener.address # IP, Port
            update_queue.put(addr)
            while True:
                # TODO: Add auth check for update.
                conn = listener.accept()
                update = conn.recv() # rp_name, timestamps
                update_queue.put(update)

    def _setup_listening_thread(self):
        self._listener_thread = threading.Thread(target=self._update_listener,
                                                 args=(self._listener_address,
                                                       self._update_queue))
        self._listener_thread.daemon = True
        self._listener_thread.start()

        try:
            # Wait for 3 seconds max to connect to establish channel.
            # If we get a value, then we know the listener worked.
            addr = self._update_queue.get(block=True, timeout=3)
        except queue.Empty:
            return None     
        return addr
    
    def _poll_for_updates(self):
        if self._polling == False:
            return
        updates = []

        # Handle MAX_UPDATES at a time, such as to not be stuck only doing
        # updates. Could also do this time based.
        for i in range(self.MAX_UPDATES):
            try:
                update = self._update_queue.get_nowait()
                assert len(update) == 2, \
                    'Updates should have the correct format.'
                updates.append(update)
            except queue.Empty as e:
                # No more updates at this time, dont need to keep polling.
                break

        for rp_name, timestamps in updates:
            handler = self._handlers.get(rp_name)
            # If handler does not exist for this update, skip.
            if handler:
                handler(rp_name, timestamps)

        # Check for updates again in POLLING_RATE_S seconds.
        update_thread = threading.Timer(self.POLLING_RATE_S,
                                        self._poll_for_updates)
        update_thread.daemon = True
        update_thread.start()

    def start_and_get_addr(self):
        return self._setup_listening_thread()
    
    def register_handler(self, rp_name:str,
                         type:Subscription,
                         handler:Callable[[str, list[int]], None]):
        if type == Subscription.NONE:
            if rp_name in self._handlers:
                del self._handlers[rp_name]
            if bool(self._handlers) == False:
                self._polling = False
        else:
            self._handlers[rp_name] = handler
            self._polling = True
            self._poll_for_updates()
