from enum import Enum

class Permission(Enum):
    NONE = 0
    READ = 1
    WRITE = 2
    READ_WRITE = 3
    OWNER = 4

class Subscription(Enum):
    NONE = 0
    ALL = 1
    CURRENT = 2
    NEXT = 3

class Priority(Enum):
    URGENT = 0
    HIGH = 1
    MEDIUM = 2
    LOW = 3

class APIRequest(Enum):
    PING = 0
    CREATE = 1
    DESTROY = 2
    COPY = 3
    CHOWN = 4
    SUBSCRIBE = 5
    READ = 6
    WRITE = 7
    GET_CONFIG = 8
    REGISTER_CHANNEL = 9
    SEND_UPDATE = 10

IOObject = list[(int, object)] # list[(timestamp, value)]