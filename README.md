# Getting Started

EnergOS is a multi-application EMS platform for the distributed, authentication-based coordination of energy resource management and control. It provides a layer of connectivity for Energy Management Systems to communicate, plan, and resolve conflict natively.

The technical architecture of EnergyOS in the current state looks like the following diagram. Using the API, individual EMS Apps are able to connect with an EnergyOS server, which is their entry into the entire EnergOS ecosystem.
<img src="prototype_diagram.png" alt="drawing" width="850"/>

### To install all dependencies and activate the environment, execute the following commands in the project's root directory.

```
conda env create -f environment.yml
conda activate energy-os-core
poetry install
```

### To run all tests (using pytest), execute the following command in the project's root directory.

```
pytest tests/
```

### To check code coverage, execute the following command in the project's root directory.

```
pytest tests/ --cov=eos
```

## General Usage

The following classes are the most relevant for most people:

- [`eos.core.API`](eos/core/api.py)
- [`eos.server.Server`](eos/server/server.py)
- [`eos.core.common.*`](eos/core/common.py)

To get an idea how these are used, and for some sample use cases, check out the [`examples`](examples/) folder.

## Interactive Playground

To run a local EOS server, you can run the [eos/server/server.py](eos/server/server.py) as a script from whichever directory you want it to run in. It will print the server address that you can use to connect with the client. We recommend making a playground/ folder to play around with this. From the root directory:

```
mkdir playground
mkdir playground/eos_server
cp samples/sample_authkeys1.txt playground/eos_server/authkeys.txt
cd playground
python ../eos/server/server.py
```

In another terminal, you can run a client. An example interactive client is provided at [sample_client.py](sample_client.py). You can use run this script and this in the playground by doing the following from the project root directory:

```
mkdir playground
cp samples/sample_auth_config1.json playground/auth_config1.json
cp samples/sample_auth_config2.json playground/auth_config2.json
cd playground
python -i ../samples/sample_client.py
```

This will ask you for both your internal server address and an app ID. Your internal server address will be printed as a string in your server. In terms of app ID, by default there are two possible IDs: 1 and 2.

### Distributed

If you want to explore the distributed nature of EnergyOS, you will need to set up another server. We recommend doing this on another device, say a Raspberry Pi. Alternatively you can run all the code (multiple servers and apps) from the same machine, if you simply want to test it out.

To do this, we will need to run another server. Open up a new terminal. Go back to the project root directory and run:
```
mkdir playground/eos_server2
cp samples/sample_authkeys2.txt playground/eos_server2/authkeys.txt
```

We will need to share the symmetric keys between the two servers, so they can authenticate between one another. Run:
```
cp samples/sample_symkeys1.txt playground/eos_server/symkeys.txt
cp samples/sample_symkeys2.txt playground/eos_server2/symkeys.txt
```

To run the new server, we can use:

```
cd playground
python ../eos/server/server.py -d eos_server2/ -a 127.0.0.1 -p 6002
```

You now have access to two more possible app IDs for interactive clients: 3 and 4. These apps are connected directly to the new server. You can run the interactive client using the same commands as above, but you will have to copy the configuration files to the playground first:

```
cp samples/sample_auth_config3.json playground/auth_config3.json
cp samples/sample_auth_config4.json playground/auth_config4.json
```

# Example Multi-App Use Case

## [Diagram](./energyos_multiapp_diagram.pdf)
To give a better visual representation of a distributed, multi-app use case, we have created the following depiction. Here, there are two apps on one computer that provide forecasts, one driver app on another computer that is responsible for communicating between EnergyOS and an external battery device, and a final app that takes this information to create a charge schedule for the external battery. Such a collaboration is made possible by using EnergyOS to coordinate resource access, information flow, and control.


# Compatibility

This software was developed and tested primarily using Unix-based systems. For any compatibility issues with your system, please contact the project authors.

# Helpful Resources
- [Conda and Poetry together](https://medium.com/@silvinohenriqueteixeiramalta/conda-and-poetry-a-harmonious-fusion-8116895b6380)
- [Multiprocessing in Python Documentation](https://docs.python.org/3/library/multiprocessing.html)
- [Python Multiprocessing: The Complete Guide](https://superfastpython.com/multiprocessing-in-python/)
- [Packaging Python Code](https://py-pkgs.org/03-how-to-package-a-python#packaging-your-code)
- [Pytest Documentation](https://docs.pytest.org/en/7.1.x/getting-started.html)

# License

Copyright (C) 2024 TUM-EMT

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
