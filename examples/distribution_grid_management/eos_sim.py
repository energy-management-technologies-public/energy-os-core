import argparse
import os
import pandas as pd

import opentumflex
from opentumflex.configuration.devices import create_device

# Simple house with PV and battery
def scenario_eos_bat(ems):
    """ EOS Battery with PV example, + boiler for satisfying heat load.

    Args:
        - ems: ems model instance

    """
    # In model.py, PV_cap is multiplied by m.solar which is our solar forecast inputted.
    # To make this run with only our forecast, we set maxpow and eta to 1.0.
    ems['devices'].update(create_device(device_name='pv', minpow=0, maxpow=1, eta=1.0))
    ems['devices'].update(create_device(device_name='bat', minpow=0, maxpow=12, stocap=20, init_soc=50, eta=0.95))
    ems['devices']['sto']['maxpow'] = 10
    ems['devices']['sto']['stocap'] = 15
    ems['devices']['boiler']['maxpow'] = 6
    return ems

def scenario_eos_ev(ems):
    """ EOS EV with PV example, + boiler for satisfying heat load.

    Args:
        - ems: ems model instance

    """
    # In model.py, PV_cap is multiplied by m.solar which is our solar forecast inputted.
    # To make this run with only our forecast, we set maxpow and eta to 1.0.
    ems['devices'].update(create_device(device_name='pv', minpow=0, maxpow=1, eta=1.0))
    # We already did this in the forecast, but also ensure here that the ev is always available.
    ems['devices'].update(create_device(device_name='ev', minpow=0, maxpow=10,
                                        stocap=20, eta=0.98, timesetting=ems['time_data'],
                                        ev_aval=['2019-12-18 00:00', '2019-12-18 23:45'],
                                        init_soc=[30], end_soc=[100]))
    ems['devices']['sto']['maxpow'] = 10
    ems['devices']['sto']['stocap'] = 15
    ems['devices']['boiler']['maxpow'] = 6
    return ems
    
def run_sim(scenario, input_file, show=False):
    path_input_data = os.path.join(os.path.abspath(os.getcwd()), input_file)
    path_results = os.path.join(os.path.abspath(os.getcwd()), 'output_otf/')

    return opentumflex.run_scenario(
                            scenario,     # Select scenario from scenario.py
                            path_input=path_input_data,              # Input path
                            path_results=path_results,               # Output path
                            solver='glpk',                           # Select solver
                            time_limit=50,                           # Time limit to solve the optimization
                            save_opt_res=False,                      # Save optimization results
                            show_opt_balance=show,                   # Plot energy balance
                            show_opt_soc=show,                      # Plot optimized SOC plan
                            show_flex_res=False,                      # Show flexibility plots
                            show_aggregated_flex=False,               # Plot aggregated flex
                            show_aggregated_flex_price='bar',        # Plot aggregated price as bar/scatter
                            save_flex_offers=False,                  # Save flexibility offers in comax/alf format
                            convert_input_tocsv=False,                # Save .xlsx file to .csv format
                            troubleshooting=False)                   # Troubleshooting on/off

if __name__ == "__main__":
    '''
    Optimization results will be saved in ems['optplan'] and flexibility offers in ems['flexopts'].
    ems['optplan'] is of type dict while ems['flexopts']['bat'] is of type dataframe.
    We can use this information to output the relevant data for the desired device to a file.

    In the 'extract_res' function in model.py in OpenTUMFlex, we have the list of the possible keys of the ems['optplan'] dict.
    Search for the 'data_input' variable. Below we include what is relevant to us.
    '''
    relevant_data = {
        'common': ['grid_import', 'grid_export', 'min cost', 'Last_elec', 'elec_supply_price', 'opt_ele_price'],
        'pv': ['PV_power', 'pv_pv2demand', 'pv_pv2grid'],
        'bat': ['bat_grid2bat', 'bat_input_power', 'bat_output_power', 'bat_SOC'],
        'ev': ['EV_power', 'EV_SOC']
    }
    options = {
        'bat': opentumflex.scenario_eos_bat,
        'ev': opentumflex.scenario_eos_ev
    }

    # Setup ArgumentParser.
    help_msg = "Choose a model (" + ", ".join(options.keys()) + ")"
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--model",
        type=str,
        required=True,
        help=help_msg,
    )

    parser.add_argument(
        "-s",
        "--show",
        action='store_true',
    )

    parser.add_argument(
        "-i",
        "--input_file",
        type=str,
        required=True,
        help='Absolute path to the input file.',
    )

    parser.add_argument(
        "-o",
        "--output_file",
        type=str,
        required=True,
        help='Absolute path to the output file.',
    )

    args = parser.parse_args()

    if args.model not in options:
        print(help_msg)
        exit()

    # Parse the args, run the simulation and output relevant data.
    scenario = options[args.model]
    ems = run_sim(scenario, args.input_file, show=args.show)
    all_data = ems['optplan']
    data_to_show = {}
    for key in relevant_data['common']:
        data_to_show[key] = all_data[key]
    for key in relevant_data['pv']:
        data_to_show[key] = all_data[key]
    for key in relevant_data[args.model]:
        data_to_show[key] = all_data[key]

    pd.DataFrame(data_to_show).to_csv(args.output_file)
    
    