import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
import os
import argparse
import pandas as pd
import matplotlib.pyplot as plt
import time
from eos.core.resource_pointer import ResourcePointer
from eos.core import API
from eos.core.common import Permission, IOObject, Subscription

class BuildingApp():
    APP_NAME = 'building'
    FORECAST_HORIZON_N = 8 # Assuming 15 min forecasts, this is 2 hours
    SIM_MAX_TIMESTEP = 96 - FORECAST_HORIZON_N # from 00:00 until 22:00
    APP_CYCLE_S = 2 # Should match the other apps in the example.
    DSO_APP_ID = 1
    DSO_LIMITS_RP = 'dso.limits'

    def __init__(self, auth_config_json, building_num):
        self._my_num = building_num
        self.APP_NAME = self.APP_NAME + str(self._my_num)
        # Only for simulation. Determines the length of a cycle for a resource.
        self._app_cycle_s = self.APP_CYCLE_S
        self._my_resources = {
            'battery_schedule': 'kW (charge-,discharge+)',
            'ev_schedule': 'kW',
            'grid_power': 'kW'
            }
        self._grid_power = 'grid_power'
        self.set_start_time()
        self.load_data()
        self._api = API(auth_config_json)
        # Set update polling rate to be faster.
        self._api.POLLING_RATE_S = 0.5
        self.updates = {}

    def set_start_time(self):
        # Round to nearest self._app_cycle_s.
        now_rounded_s = (self._app_cycle_s *
                  round(ResourcePointer.GetNowSeconds() / self._app_cycle_s))
        # Set in the future to ensure valid writes.
        # This should also align with the start times for the other apps in the
        # example.
        self._start_time_s = now_rounded_s + self._app_cycle_s

    def load_data(self):
        # Load timeseries
        self._data = pd.DataFrame()
        for resource, col in self._my_resources.items():
            if resource == 'grid_power':
                continue
            path = os.path.join(app_dir, resource + '.csv')
            timeseries = pd.read_csv(path, sep=',', index_col=0)
            self._data[self.get_col_name(resource)] = timeseries[col]
        # Make grid_power
        self._data[self.get_col_name('grid_power')] = \
            self._data[self.get_col_name('ev_schedule')] + \
            -self._data[self.get_col_name('battery_schedule')]
        # Add timestamps
        self._data['timestamp'] = \
            self._data.index.map(
                lambda t: self._start_time_s + t*self._app_cycle_s)
        
    def get_rp_name(self, resource):
        return self.APP_NAME + '.' + resource
    
    def get_resource_name(self, rp_name):
        return rp_name[len(self.APP_NAME) + 1:]
    
    def get_col_name(self, resource):
        return resource + ' (' + self._my_resources[resource] + ')'

    def setup_resources(self):
        rp_name = self.get_rp_name(self._grid_power)
        self._api.Create(rp_name, self._app_cycle_s)
        # Make this RP readable by DSO App.
        self._api.Chown(rp_name, self.DSO_APP_ID,
                        permission=Permission.READ)
                
    def setup_subscriptions(self, subscriptions:list[(str, Subscription)]):
        success = [False] * len(subscriptions)
        attempts = 0
        # Try to subscribe to RP. May not have been created yet.
        while attempts < 3:
            for i, sub in enumerate(subscriptions):
                rp_name, sub_type = sub
                if success[i]:
                    continue
                done = self._api.Subscribe(rp_name,
                                                 sub_type,
                                                 self.handle_new_update)
                if done:
                    success[i] = done
                    print(self.APP_NAME.upper() + ': Subscribed to ' +
                          rp_name + '.')
            if all(success):
                break
            attempts += 1
            time.sleep(0.2)

    def handle_new_update(self, rp_name, timestamps:list[int]):
        # Update received for resource.
        min_t = timestamps[0]
        max_t = min_t + self._app_cycle_s*self.FORECAST_HORIZON_N
        print(self.APP_NAME.upper() + ': Update received for ' +
              rp_name + ' (' + str(min_t) + ',' + str(max_t) +').')

        # Only updates once if we have subscribed to updates for mutiple RPs.
        t = self.updates.get(min_t)
        if t:
            return
        else:
            self.updates[min_t] = True
        # Can now update its schedule.
        self.update_schedule(rp_name, min_t, max_t)

    def read_resource(self, resource, start_time_s, end_time_s):
        rp_name = self.get_rp_name(resource)
        return self._api.Read(rp_name, start_time_s, end_time_s)

    def write_resource(self, resource, data:IOObject):
        rp_name = self.get_rp_name(resource)
        return self._api.Write(rp_name, list(data))

    def update_schedule(self, rp_name, min_t, max_t):
        success, limits = self._api.Read(rp_name, min_t, max_t)
        if not success:
            print(self.APP_NAME.upper() + ': Cannot read limits.')
            return
        plan = self.make_schedule(self._grid_power, min_t, max_t)
        # Resolution algorithm is naive: splits load equally.
        equal_load = (sum([plan_t[1] for plan_t in plan]) /
                      self.FORECAST_HORIZON_N)
        lost_energy = 0
        new_plan = []
        for t, limit in limits:
            new_plan.append((t, min(equal_load, limit)))
            lost_energy += max(equal_load - limit, 0)
        
        resource = self._grid_power
        if self.write_resource(resource, new_plan):
            extra = ''
            if lost_energy > 0:
                extra = ' Gave up ' + str(lost_energy) + 'kW between (' + \
                str(min_t) + ',' + str(max_t) +').'
            print(self.APP_NAME.upper() +
                ': Wrote optimized schedule for ' +
                self.get_rp_name(resource) + '.' + extra)
        else:
            print(self.APP_NAME.upper() +
                  ': Failed to write schedule for ' +
                  self.get_rp_name(resource) + '.')

    def make_schedule(self, resource, min_t, max_t):
        # Simulate time it takes to make charge schedule.
        # In a real-situation, you would perform the optimization.
        time.sleep(0.1)
        col = self.get_col_name(resource)
        condition = self._data['timestamp'].between(min_t, max_t,
                                                    inclusive='left')
        schedule = list(zip(self._data['timestamp'][condition],
                            self._data[col][condition]))
        return schedule
    
    def sleep_until(self, start_next_cycle):
        now_s = ResourcePointer.GetNowSeconds()
        diff_s = start_next_cycle - now_s
        assert diff_s >= 0, 'You need to set a longer app cycle.'
        time.sleep(diff_s)

    def run(self):
        print(self.APP_NAME.upper() + ': Start time ', self._start_time_s)
        self.setup_resources()
        start_next_cycle = self._start_time_s
        self.sleep_until(start_next_cycle)
        self.setup_subscriptions([(self.DSO_LIMITS_RP, Subscription.ALL)])
        t = 0
        while t < self.SIM_MAX_TIMESTEP:
            print(self.APP_NAME.upper() + ': Step',
                  str(t+1) + '/' + str(self.SIM_MAX_TIMESTEP))
            min_t = start_next_cycle
            max_t = min_t + self._app_cycle_s*self.FORECAST_HORIZON_N
            t += 1
            resource = self._grid_power
            entries = self.make_schedule(resource, min_t, max_t)
            
            if self.write_resource(resource, entries):
                print(self.APP_NAME.upper() +
                    ': Wrote schedule for grid power forecast for ' +
                    self.get_rp_name(resource) + '.')
            else:
                print(self.APP_NAME.upper() +
                    ': Failed to write forecast for ' +
                    self.get_rp_name(resource) + '.')

            start_next_cycle = start_next_cycle + self._app_cycle_s
            self.sleep_until(start_next_cycle)

        success, schedule = self.read_resource(resource,
                                               self._start_time_s,
                                               start_next_cycle)
        if success:
            x, y = zip(*schedule)
            plt.plot(x, y)
            plt.title(str(self._my_num) + ' ' + self.get_col_name(resource))
            path = os.path.join(app_dir, 'results.png')
            plt.savefig(path)
            plt.show()
            print(self.APP_NAME.upper() + ': Done. Results saved to', path)
        else:
            print(self.APP_NAME.upper() + ': Error. No results to show.')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n",
        "--num",
        type=int,
        required=True,
        help='Need to pass in a building number.',
    )
    args = parser.parse_args()
    app_dir = os.path.dirname(os.path.realpath(__file__))
    auth_config_json = os.path.join(app_dir,
                                    'auth_config_' + str(args.num) + '.json')
    app = BuildingApp(auth_config_json, args.num)
    app.run()

