# Overview

In this example, we illustrate a DSO trying to manage its distribution grid of n buildings. Each building plans a schedule of its required power from the grid, and the DSO checks this against the maximum Transformer load.

<img src="distribution_grid_management_image.png" alt="drawing" width="500"/>

# Simulation Progression

In this case, we have n buildings attached to the grid, each with their own local EnergyOS instances. As part of their provision agreement, the Distribution System Operator (DSO) installs a building app that is accessible from their local EnergyOS, which stores planned grid power into a Resource Pointer. The grid power Resource Pointer accumulates the results of other schedules in the building, such as those developed in the Household Load Scheduling example in the other folder. The remote grid operator subscribes to this Resource Pointer. Additionally, it creates a building-accessible Resource Pointer called limits, detailing the cumulative power load that buildings can have for a certain period of time, and its own private maximum transformer load Resource Pointer, to keep track of transformer utilization. When there are changes to grid power from the building, the DSO app receives an update, accumulates the schedules of all buildings in its grid, and sends out limits if necessary. The DSO in this case has priority, and can cut off or lower grid power to a building if the limit is not adhered to. In EnergyOS, each app in the building's EnergyOS would subscribe to this limits Resource Pointer from the DSO. Upon receiving new limits, the apps would re-calculate their schedules if necessary.

# Run the code

Run the code by navigating to this folder and running the following command. The simulation will run and at the end it will display and save figures.

```
python run.py -n <number_of_buildings>
```
Note: Make sure the APP_CYCLE_MS variable matches between apps in this example.