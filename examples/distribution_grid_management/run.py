import signal
import sys
import subprocess
import os
import argparse
import json
import time
import secrets
from eos.server import Server

def kill_processes(sig, frame):
    for p in processes:
        p.terminate()
    print('\nYou pressed Ctrl+C! Terminating server and apps..\n')
    sys.exit(0)

def eos_server_setup_and_path():
    authkeys_file = os.path.join(server_base_dir, Server.AUTHKEYS_FILE)
    with open(authkeys_file, 'w') as f:
        for config in [DSO_CONFIG] + APPS:
            app_id, authkey = config
            f.write(':'.join([str(app_id), authkey]) + '\n')
    path = os.path.join(os.getcwd(), '..', '..', 'eos', 'server', 'server.py')
    return path

def app_setup_and_path(name:str, app_id:int, authkey:str):
    auth = {
        'server_addr': Server.GetInternalServerAddr(server_base_dir),
        'app_id': app_id,
        'authkey': authkey
    }
    app_base_dir = os.path.join(os.getcwd(), name)
    auth_config = os.path.join(app_base_dir, 'auth_config_' + str(app_id - 1) + '.json')
    with open(auth_config, 'w') as f:
        json.dump(auth, f)
    
    path = os.path.join(app_base_dir, 'app.py')
    return path

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n",
        "--num",
        type=int,
        required=True,
        help='Need to pass in the number of buildings in this grid.',
    )
    args = parser.parse_args()
    signal.signal(signal.SIGINT, kill_processes)

    DSO_CONFIG = (1, secrets.token_hex(10))
    APPS = [(id, secrets.token_hex(10)) for id in range(2, 2 + args.num)]
    # Construct server address. Normally, this would just be running at a fixed
    # address on the machine, and apps would get the address after being
    # installed.
    server_base_dir = os.path.join(os.getcwd(), Server.EOS_SERVER_DIR)
    if not os.path.isdir(server_base_dir):
        os.mkdir(server_base_dir)

    processes = []
    processes.append(subprocess.Popen(['python',
                                       eos_server_setup_and_path()]))
    
    time.sleep(1)
    
    # Set up DSO app.
    path = app_setup_and_path('dso_app', DSO_CONFIG[0], DSO_CONFIG[1])
    processes.append(subprocess.Popen(['python', path, '-n ' + str(args.num)]))

    # Set up building apps.
    for app_id, authkey in APPS:
        n = app_id - 1
        path = app_setup_and_path('building_app', app_id, authkey)
        processes.append(subprocess.Popen(['python', path, '-n ' + str(n)]))
    
    # Wait for the apps to finish running
    for p in processes[1:]:
        p.wait()

    # Server loops forever, so can just terminate.
    processes[0].terminate()
