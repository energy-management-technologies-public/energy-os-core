import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
import os
import argparse
import pandas as pd
import matplotlib.pyplot as plt
import time
from eos.core.resource_pointer import ResourcePointer
from eos.core import API
from eos.core.common import Permission, IOObject, Subscription

class DSOApp():
    APP_NAME = 'dso'
    FORECAST_HORIZON_N = 8 # Assuming 15 min forecasts, this is 2 hours
    SIM_MAX_TIMESTEP = 96 - FORECAST_HORIZON_N # from 00:00 until 22:00
    APP_CYCLE_S = 2 # Should match the other apps in the example.
    BUILDING_APP_NAME = 'building'
    BUILDING_APP_RP_NAME = 'grid_power'

    def __init__(self, auth_config_json, num_buildings):
        self._num_buildings = num_buildings
        self._building_app_ids = list(range(2, 2 + self._num_buildings))
        # Only for simulation. Determines the length of a cycle for a resource.
        self._app_cycle_s = self.APP_CYCLE_S
        self._my_resources = {
            'limits': 'kW',
            'max_load': 'kW',
            'total_grid_power': 'kW',
            }
        self.set_start_time()
        self.load_data()
        self._api = API(auth_config_json)
        # Set update polling rate to be faster.
        self._api.POLLING_RATE_S = 0.5
        self.updates = {}

    def set_start_time(self):
        # Round to nearest self._app_cycle_s.
        now_rounded_s = (self._app_cycle_s *
                  round(ResourcePointer.GetNowSeconds() / self._app_cycle_s))
        # Set in the future to ensure valid writes.
        # This should also align with the start times for the other apps in the
        # example.
        self._start_time_s = now_rounded_s + self._app_cycle_s

    def load_data(self):
        # Load timeseries
        self._data = pd.DataFrame()
        for resource, col in self._my_resources.items():
            # The others we will construct.
            if resource != 'limits':
                continue
            path = os.path.join(app_dir, resource + '.csv')
            timeseries = pd.read_csv(path, sep=',', index_col=0)
            self._data[self.get_col_name(resource)] = timeseries[col]
        # Make max_load. Add up limits between buildings.
        self._data[self.get_col_name('max_load')] = \
            self._data[self.get_col_name('limits')] * self._num_buildings
        # Add timestamps
        self._data['timestamp'] = \
            self._data.index.map(
                lambda t: self._start_time_s + t*self._app_cycle_s)
        
    def get_rp_name(self, resource):
        return self.APP_NAME + '.' + resource
    
    def get_resource_name(self, rp_name):
        return rp_name[len(self.APP_NAME) + 1:]
    
    def get_col_name(self, resource):
        return resource + ' (' + self._my_resources[resource] + ')'

    def setup_resources(self):
        rp_name = self.get_rp_name('limits')
        self._api.Create(rp_name, self._app_cycle_s)
        for id in self._building_app_ids:
            self._api.Chown(rp_name, id, permission=Permission.READ)
        rp_name = self.get_rp_name('total_grid_power')
        self._api.Create(rp_name, self._app_cycle_s)
                
    def setup_subscriptions(self, subscriptions:list[(str, Subscription)]):
        success = [False] * len(subscriptions)
        attempts = 0
        # Try to subscribe to RP. May not have been created yet.
        while attempts < 3:
            for i, sub in enumerate(subscriptions):
                rp_name, sub_type = sub
                if success[i]:
                    continue
                done = self._api.Subscribe(rp_name,
                                                 sub_type,
                                                 self.handle_new_update)
                if done:
                    success[i] = done
                    print(self.APP_NAME.upper() + ': Subscribed to ' +
                          rp_name + '.')
            if all(success):
                break
            attempts += 1
            time.sleep(0.2)

    def handle_new_update(self, rp_name, timestamps:list[int]):
        # Update received for resource.
        min_t = timestamps[0]
        max_t = min_t + self._app_cycle_s*self.FORECAST_HORIZON_N
        print(self.APP_NAME.upper() + ': Update received for ' +
              rp_name + ' (' + str(min_t) + ',' + str(max_t) +').')

        # Only updates once if we have subscribed to updates for mutiple RPs.
        t = self.updates.get(min_t)
        if not t:
            t = 0
        t += 1
        self.updates[min_t] = t
        if t % self._num_buildings == 0:
            # Skip if we have not yet received all buildings.
            # Note this is a simulation and we will get all writes twice if the
            # limit was crossed, because the buildings will re-write.
            # Each time we can see if a boundary has been crossed.
            self.update_schedule(rp_name, min_t, max_t)
            return
    
    def read_resource(self, resource, start_time_s, end_time_s):
        rp_name = self.get_rp_name(resource)
        return self._api.Read(rp_name, start_time_s, end_time_s)

    def write_resource(self, resource, data:IOObject):
        rp_name = self.get_rp_name(resource)
        return self._api.Write(rp_name, list(data))

    def update_schedule(self, rp_name, min_t, max_t):
        success, grid_power = self._api.Read(rp_name, min_t, max_t)
        if not success:
            print(self.APP_NAME.upper() + ': Cannot read grid_power.')
            return
        
        max_loads = self.get_schedule('max_load', min_t, max_t)
        power_totals = []
        limit_crossed = False
        for grid_power_t, max_loads_t in zip(grid_power, max_loads):
            power_total = grid_power_t[1]*self._num_buildings
            max_load = max_loads_t[1]
            if (power_total > max_load):
                limit_crossed = True
                break
            # Will not finish power totals if a boundary was crossed. This will
            # not get written anyways.
            power_totals.append((grid_power_t[0], power_total))
        
        if limit_crossed:
            limits = self.get_schedule('limits', min_t, max_t)
            print(self.APP_NAME.upper() +
                  ': Limit crossed. Sending new limits for ' +
                  '(' + str(min_t) + ',' + str(max_t) +').')
            self.write_resource('limits', limits)
        else:
            print(self.APP_NAME.upper() +
                  ': Success. Grid power is less than the max load for ' +
                  '(' + str(min_t) + ',' + str(max_t) +').')
            self.write_resource('total_grid_power', power_totals)

    def get_schedule(self, resource, min_t, max_t):
        # Simulate time it takes to make charge schedule.
        # In a real-situation, you would perform the optimization.
        col = self.get_col_name(resource)
        condition = self._data['timestamp'].between(min_t, max_t,
                                                    inclusive='left')
        schedule = list(zip(self._data['timestamp'][condition],
                            self._data[col][condition]))
        return schedule
    
    def sleep_until(self, start_next_cycle):
        now_s = ResourcePointer.GetNowSeconds()
        diff_s = start_next_cycle - now_s
        assert diff_s >= 0, 'You need to set a longer app cycle.'
        time.sleep(diff_s)

    def run(self):
        print(self.APP_NAME.upper() + ': Start time ', self._start_time_s)
        self.setup_resources()
        start_next_cycle = self._start_time_s
        self.sleep_until(start_next_cycle)
        self.setup_subscriptions([
            (self.BUILDING_APP_NAME + str(i-1) + '.' + self.BUILDING_APP_RP_NAME,
             Subscription.ALL)
             for i in self._building_app_ids])
        t = 0
        while t < self.SIM_MAX_TIMESTEP:
            print(self.APP_NAME.upper() + ': Step',
                  str(t+1) + '/' + str(self.SIM_MAX_TIMESTEP))
            min_t = start_next_cycle
            max_t = min_t + self._app_cycle_s*self.FORECAST_HORIZON_N
            t += 1
            # Can do whatever other operations we want, or even set a high
            # limit at first. But we actually only need to react to changes
            # in grid_power.
            start_next_cycle = start_next_cycle + self._app_cycle_s
            self.sleep_until(start_next_cycle)
        
        resource = 'total_grid_power'
        success, schedule = self.read_resource(resource,
                                               self._start_time_s,
                                               start_next_cycle)
        if success:
            x, y = zip(*schedule)
            plt.plot(x, y)
            plt.title(self.get_col_name(resource))
            path = os.path.join(app_dir, 'results.png')
            plt.savefig(path)
            plt.show()
            print(self.APP_NAME.upper() + ': Done. Results saved to', path)
        else:
            print(self.APP_NAME.upper() + ': Error. No results to show.')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n",
        "--num",
        type=int,
        required=True,
        help='Need to pass in the number of buildings in this grid.',
    )
    args = parser.parse_args()
    app_dir = os.path.dirname(os.path.realpath(__file__))
    auth_config_json = os.path.join(app_dir, 'auth_config_0.json')
    app = DSOApp(auth_config_json, args.num)
    app.run()

