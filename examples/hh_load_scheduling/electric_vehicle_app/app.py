import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
import os
import pandas as pd
import matplotlib.pyplot as plt
import time
from eos.core.resource_pointer import ResourcePointer
from eos.core import API
from eos.core.common import Permission, IOObject, Subscription

class ElectricVehicleApp():
    APP_NAME = 'ev'
    FORECAST_HORIZON_N = 8 # Assuming 15 min forecasts, this is 2 hours
    SIM_MAX_TIMESTEP = 96 - FORECAST_HORIZON_N # from 00:00 until 22:00
    APP_CYCLE_S = 2 # Should match the other apps in the example.
    BATTERY_APP_ID = 2
    BATTERY_APP_LOAD_RP = 'battery.load_forecast'
    BATTERY_APP_PV_RP = 'battery.pv_forecast'

    def __init__(self, auth_config_json):
        # Only for simulation. Determines the length of a cycle for a resource.
        self._app_cycle_s = self.APP_CYCLE_S
        self._my_resources = {
            'ev_schedule': 'kW'
            }
        self.set_start_time()
        self.load_data()
        self._api = API(auth_config_json)
        # Set update polling rate to be faster.
        self._api.POLLING_RATE_S = 0.5
        self.updates = {}

    def set_start_time(self):
        # Round to nearest self._app_cycle_s.
        now_rounded_s = (self._app_cycle_s *
                  round(ResourcePointer.GetNowSeconds() / self._app_cycle_s))
        # Set to self._app_cycle_s*2 in the future to ensure valid writes.
        # This should also align the start times for the other apps in the
        # example.
        self._start_time_s = now_rounded_s + self._app_cycle_s

    def load_data(self):
        self._data = pd.DataFrame()
        # Load timeseries
        for resource, col in self._my_resources.items():
            path = os.path.join(app_dir, resource + '.csv')
            timeseries = pd.read_csv(path, sep=',', index_col=0)
            self._data[self.get_col_name(resource)] = timeseries[col]
        # Add timestamps
        self._data['timestamp'] = \
            self._data.index.map(
                lambda t: self._start_time_s + t*self._app_cycle_s)
        
    def get_rp_name(self, resource):
        return self.APP_NAME + '.' + resource
    
    def get_resource_name(self, rp_name):
        return rp_name[len(self.APP_NAME) + 1:]
    
    def get_col_name(self, resource):
        return resource + ' (' + self._my_resources[resource] + ')'

    def setup_resources(self):
        for resource, _ in self._my_resources.items():
            rp_name = self.get_rp_name(resource)
            self._api.Create(rp_name, self._app_cycle_s)
            # Make this RP readable by Forecasting App.
            self._api.Chown(rp_name, self.BATTERY_APP_ID,
                            permission=Permission.READ)

    def setup_subscriptions(self, subscriptions:list[(str, Subscription)]):
        success = [False] * len(subscriptions)
        attempts = 0
        # Try to subscribe to RP. May not have been created yet.
        while attempts < 3:
            for i, sub in enumerate(subscriptions):
                rp_name, sub_type = sub
                if success[i]:
                    continue
                done = self._api.Subscribe(rp_name,
                                                 sub_type,
                                                 self.handle_new_update)
                if done:
                    success[i] = done
                    print(self.APP_NAME.upper() + ': Subscribed to ' +
                          rp_name + '.')
            if all(success):
                break
            attempts += 1
            time.sleep(0.2)

    def handle_new_update(self, rp_name, timestamps:list[int]):
        # Update received for resource.
        min_t = timestamps[0]
        max_t = min_t + self._app_cycle_s*self.FORECAST_HORIZON_N
        print(self.APP_NAME.upper() + ': Update received for ' +
              rp_name + ' (' + str(min_t) + ',' + str(max_t) +').')
        
        # Can read updates using:
        # self._api.Read(rp_name, min_t, max_t)

        # Only updates once if we have subscribed to updates for mutiple RPs.
        t = self.updates.get(min_t)
        if t:
            return
        else:
            self.updates[min_t] = True
        # Can now update its schedule.
        self.update_schedule('ev_schedule', min_t, max_t)

    def read_resource(self, resource, start_time_s, end_time_s):
        rp_name = self.get_rp_name(resource)
        return self._api.Read(rp_name, start_time_s, end_time_s)

    def write_resource(self, resource, data:IOObject):
        rp_name = self.get_rp_name(resource)
        return self._api.Write(rp_name, list(data))

    def update_schedule(self, resource, min_t, max_t):
        entries = self.make_schedule(resource, min_t, max_t)
        if self.write_resource(resource, entries):
            print(self.APP_NAME.upper() +
                ': Wrote optimized schedule for ' +
                self.get_rp_name(resource) + '.')
        else:
            print(self.APP_NAME.upper() +
                  ': Failed to write schedule for ' +
                  self.get_rp_name(resource) + '.')

    def make_schedule(self, resource, min_t, max_t, naive=False):
        # Simulate time it takes to make charge schedule.
        # In a real-situation, you would perform the optimization.
        time.sleep(0.1)
        if naive:
            return [(t, 0) for t in range(min_t, max_t, self._app_cycle_s)]
        col = self.get_col_name(resource)
        condition = self._data['timestamp'].between(min_t, max_t,
                                                    inclusive='left')
        schedule = list(zip(self._data['timestamp'][condition],
                            self._data[col][condition]))
        return schedule

    def sleep_until(self, start_next_cycle):
        now_s = ResourcePointer.GetNowSeconds()
        diff_s = start_next_cycle - now_s
        assert diff_s >= 0, 'You need to set a longer app cycle.'
        time.sleep(diff_s)

    def run(self):
        print(self.APP_NAME.upper() + ': Start time ', self._start_time_s)
        self.setup_resources()
        start_next_cycle = self._start_time_s
        self.sleep_until(start_next_cycle)
        self.setup_subscriptions([(self.BATTERY_APP_LOAD_RP,
                                   Subscription.ALL),
                                  (self.BATTERY_APP_PV_RP,
                                   Subscription.ALL)])
        t = 0
        while t < self.SIM_MAX_TIMESTEP:
            print(self.APP_NAME.upper() + ': Step',
                  str(t+1) + '/' + str(self.SIM_MAX_TIMESTEP))
            min_t = start_next_cycle
            max_t = min_t + self._app_cycle_s*self.FORECAST_HORIZON_N
            t += 1
            resource = 'ev_schedule'
            # Can do whatever other operations we want, or even do a bad
            # schedule beforehand. The real schedules happen after we get
            # updates.
            _ = self.make_schedule(resource, min_t, max_t, naive=True)
            # self.write_resource(resource, _)
            print(self.APP_NAME.upper() +
                  ': Wrote dumb schedule for ' +
                  self.get_rp_name(resource) + '.')

            start_next_cycle = start_next_cycle + self._app_cycle_s
            self.sleep_until(start_next_cycle)
        
        resource = 'ev_schedule'
        success, schedule = self.read_resource(resource,
                                               self._start_time_s,
                                               start_next_cycle)
        if success:
            x, y = zip(*schedule)
            plt.plot(x, y)
            plt.title(self.get_col_name(resource))
            path = os.path.join(app_dir, 'results.png')
            plt.savefig(path)
            plt.show()
            print(self.APP_NAME.upper() + ': Done. Results saved to', path)
        else:
            print(self.APP_NAME.upper() + ': Error. No results to show.')

if __name__ == '__main__':
    app_dir = os.path.dirname(os.path.realpath(__file__))
    auth_config_json = os.path.join(app_dir, 'auth_config.json')
    app = ElectricVehicleApp(auth_config_json)
    app.run()

