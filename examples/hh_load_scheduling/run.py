import signal
import sys
import subprocess
import os
import json
import time
import secrets
from eos.server import Server

def kill_processes(sig, frame):
    for p in processes:
        p.terminate()
    print('\nYou pressed Ctrl+C! Terminating server and apps..\n')
    sys.exit(0)

def eos_server_setup_and_path():
    authkeys_file = os.path.join(server_base_dir, Server.AUTHKEYS_FILE)
    with open(authkeys_file, 'w') as f:
        for _, config in APPS.items():
            app_id, authkey = config
            f.write(':'.join([str(app_id), authkey]) + '\n')
    path = os.path.join(os.getcwd(), '..', '..', 'eos', 'server', 'server.py')
    return path

def app_setup_and_path(name:str, app_id:int, authkey:str):
    auth = {
        'server_addr': Server.GetInternalServerAddr(server_base_dir),
        'app_id': app_id,
        'authkey': authkey
    }
    app_base_dir = os.path.join(os.getcwd(), name)
    auth_config = os.path.join(app_base_dir, 'auth_config.json')
    with open(auth_config, 'w') as f:
        json.dump(auth, f)
    
    path = os.path.join(app_base_dir, 'app.py')
    return path

if __name__ == '__main__':
    signal.signal(signal.SIGINT, kill_processes)
    APPS = {
        'battery_app': (2, secrets.token_hex(10)),
        'electric_vehicle_app': (3, secrets.token_hex(10))
    }
    # Construct server address. Normally, this would just be running at a fixed
    # address on the machine, and apps would get the address after being
    # installed.
    server_base_dir = os.path.join(os.getcwd(), Server.EOS_SERVER_DIR)
    if not os.path.isdir(server_base_dir):
        os.mkdir(server_base_dir)

    processes = []
    processes.append(subprocess.Popen(['python',
                                       eos_server_setup_and_path()]))
    time.sleep(1)
    for app_name, config in APPS.items():
        path = app_setup_and_path(app_name, config[0], config[1])
        processes.append(subprocess.Popen(['python', path]))
    
    # Wait for the apps to finish running
    for p in processes[1:]:
        p.wait()

    # Server loops forever, so can just terminate.
    processes[0].terminate()
