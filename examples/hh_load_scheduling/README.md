# Overview

In this example, we illustrate a battery app and an electric vehicle app that coordinate with each other to create more optimized forecasts. Each one is aware of the other's public resource pointers and subscribes to updates from them, in order to improve their individual charge plans.

<img src="hh_load_schedling_image.png" alt="drawing" width="500"/>

# Simulation Progression

In order to plan an optimized charge schedule for the battery storage system (BSS), the battery app performs sophisticated forecasts for electricity local electricity load and local solar generation. It takes this information, creates a schedule, and also puts the forecasts in Resource Pointers with read access for the electric vehicle (EV) app in the local EnergyOS. The EV app is aware of the forecast Resource Pointers and subscribes to updates from them. These forecasts would greatly improve the app's EV charging schedule because previously it had to rely on standard load and solar profiles. In return, the EV app creates a Resource Pointer for its charging schedule, which the battery app subscribes to. When the forecasts are written, the EV app is notified and uses these new values to provide an optimized charging schedule for the electric vehicle. This schedule is then written to the EV schedule Resource Pointer, which triggers an update for the battery app. With this new information, the battery app can further optimize the BSS charging schedule, as it is aware of new loads in the environment.

# Run the code

Run the code by navigating to this folder and running the following command. The simulation will run and at the end it will display and save the schedules of the two devices.

```
python run.py
```
Note: Make sure the APP_CYCLE_MS variable matches between apps in this example.

# Analysis

If you want to understand the code we used in this simulation, and why it is useful, you can play around with the [eos_analysis.ipynb](./eos_analysis.ipynb) notebook provided, in which we analyze the schedules outputted by the simulation.

You can additionally see how the data was prepared, or even generate new simulation data, using the [eos_data.ipynb](./eos_data.ipynb) notebook.

The data includes those in the following folders.

`input/`: Contains the data for the optimizations performed by both apps. \
`output/`: Contains the output data from the optimizations performance by both apps. \
`data/`: Contains the EOS-relevant data from the optimization outputs.