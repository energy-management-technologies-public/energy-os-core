import os
import json
import pytest
import asyncio
from unittest.mock import Mock
from multiprocessing import Process, Queue
import string
import random
from eos.core.resource_pointer import ResourcePointer
from eos.core.common import (Permission,
                             Subscription,
                             Priority)
from eos.server import Server
from eos.core import API

class TestAPI:
    """
    Test Class for the API functions. Involves creating a server in a separate
    process to listen to requests. If the tests die due to an error in the
    multiprocessing python library, this may be due to the server. Run test with
    'pytest -s' in order to view standard output (including from server
    process).
    """
    def setup_class(self):
        # Create base location for EOS server
        dir_path = os.path.dirname(os.path.realpath(__file__))
        self.test_output_dir = os.path.join(dir_path, 'eos_test_server')
        if not os.path.exists(self.test_output_dir):
            os.mkdir(self.test_output_dir)
        self.auth = {
            'server_addr': Server.GetInternalServerAddr(self.test_output_dir),
            'app_id': 1,
            'authkey': 'secretpassword'
        }

        # Get the app's auth file.
        self.auth_config = os.path.join(dir_path,
                                      'test_auth_config.json')
        with open(self.auth_config, 'w') as f:
            json.dump(self.auth, f)

        # Create authkey file for the server.
        authkeys_file = os.path.join(self.test_output_dir, Server.AUTHKEYS_FILE)
        with open(authkeys_file, 'w') as f:
            f.write(':'.join([str(self.auth['app_id']), self.auth['authkey']]))

        # Set root dir for resource pointers so that it matches the server's
        # root dir.
        ResourcePointer.SetRootDir(os.path.join(self.test_output_dir,
                                Server.RP_SUBDIR))
    
    def remove_resourcefiles(self):
        if not os.path.exists(ResourcePointer.ROOT_DIR):
            return
        for name in os.listdir(ResourcePointer.ROOT_DIR):
            f = os.path.join(ResourcePointer.ROOT_DIR, name)
            os.remove(f)

    # This function is running in a separate process. Do not try to access or
    # change its values.
    def start_server(self, q, output_dir):
        s = Server(eos_server_dir_absolute=output_dir)
        s.start(lambda: q.put(True))

    def setup_method(self):
        self.remove_resourcefiles()
        # Starts the server in a different process, sends a message to the queue
        # when it is up and running, and then creates the API to connect to it.
        q = Queue()
        self.p = Process(target=self.start_server, args=(q, self.test_output_dir))
        self.p.start()
        q.get()
        self.API = API(self.auth_config)

    def teardown_method(self):
        self.p.terminate()
        # self.remove_resourcefiles()

    def test_api_ping(self):
        assert self.API.Ping() == True

    def test_api_create_and_get_config(self):
        resource_name = 'my_resource'
        cycle = 2 # should be 2 or more seconds here
        invalid_characters = list(
            set(string.punctuation + string.whitespace) - \
            ResourcePointer.ALLOWED_CHARACTERS
        )

        assert self.API.Create(
                            resource_name + random.choice(invalid_characters),
                            cycle) == False, \
            'The Create call should be unsuccessful.'

        assert self.API.Create(resource_name, cycle) == True, \
            'The Create call should be successful.'

        assert self.API.Create(resource_name, cycle) == False, \
            'Creating the RP should fail because resource exists.'
        
        success, config = self.API.GetConfig(resource_name)
        assert success == True, \
            'The Config call should be successful.'

        assert config != None, \
            'The config should be a valid dictionary.'
        
        assert config['name'] == resource_name, \
            'The name of the resource should match.'
        
        assert config['cycle_s'] == cycle, \
            'The cycle of the resource should match.'
        
        assert config['is_link'] == False, \
            'The resource should not be a link.'
        
        expected_access = {
            'id': self.auth['app_id'],
            'permission': Permission.OWNER.name,
            'priority': Priority.MEDIUM.name,
            'subscription': Subscription.NONE.name
        }
        real_access_list = config['access_list']
        assert (len(real_access_list) == 1 and
                expected_access in real_access_list), \
            'The access lists should match.'
        
    def test_api_destroy(self):
        resource_name = 'my_resource'
        cycle = 2 # should be 2 or more seconds here

        assert self.API.Destroy(resource_name) == False, \
            ('The Destroy call should not be successful because there is no '
             'Resource Pointer to destroy.')

        assert self.API.Create(resource_name, cycle) == True, \
            'The Create call should be successful.'
        
        assert self.API.Create(resource_name, cycle) == False, \
            'Creating the RP should fail because resource exists.'
        
        assert self.API.Destroy(resource_name) == True, \
            'The Destroy call should be successful.'
        
        assert self.API.Create(resource_name, cycle) == True, \
            ('Creating the RP should succeed because the old resource was '
             'destroyed')
        
    def test_api_chown(self):
        resource_name = 'my_resource'
        cycle = 2 # should be 2 or more seconds here
        new_app_id = self.auth['app_id'] + 1

        assert self.API.Chown(resource_name, new_app_id) == False, \
            'The Chown call should fail because the resource does not exist.'

        assert self.API.Create(resource_name, cycle) == True, \
            'The Create call should be successful.'
        
        owner_access = {
            'id': self.auth['app_id'],
            'permission': Permission.OWNER.name,
            'priority': Priority.MEDIUM.name,
            'subscription': Subscription.NONE.name
        }

        success, config = self.API.GetConfig(resource_name)
        assert success == True, \
            'The Config call should be successful.'
        
        real_access_list = config['access_list']
        assert (len(real_access_list) == 1 and
                owner_access in real_access_list), \
            'The access lists should match.'
        
        assert self.API.Chown(resource_name, new_app_id) == True, \
            'The Chown call should be successful.'

        new_app_access = {
            'id': new_app_id,
            'permission': Permission.NONE.name,
            'priority': Priority.LOW.name,
            'subscription': Subscription.NONE.name
        }

        _, config = self.API.GetConfig(resource_name)
        real_access_list = config['access_list']
        assert (len(real_access_list) == 2 and
                owner_access in real_access_list and
                new_app_access in real_access_list), \
            'The new app should be added to the access list.'
        

        assert self.API.Chown(resource_name,
                              new_app_id,
                              permission=Permission.READ) == True, \
            'The Chown call should be successful.'
        _, config = self.API.GetConfig(resource_name)
        new_app_access['permission'] = Permission.READ.name
        real_access_list = config['access_list']
        assert (len(real_access_list) == 2 and
                new_app_access in real_access_list), \
            'The new app should have its permission be changed.'
        
        assert self.API.Chown(resource_name,
                              new_app_id,
                              priority=Priority.MEDIUM) == True, \
            'The Chown call should be successful.'
        _, config = self.API.GetConfig(resource_name)
        new_app_access['priority'] = Priority.MEDIUM.name
        real_access_list = config['access_list']
        assert (len(real_access_list) == 2 and
                new_app_access in real_access_list), \
            'The new app should have its priority be changed.'
        
    def test_api_read_write(self):
        resource_name = 'my_resource'
        cycle = 2 # should be 2 or more seconds here
        current_cycle = int(ResourcePointer.GetNowSeconds())
        next_cycle = current_cycle + cycle
        prev_cycle = current_cycle - cycle

        assert self.API.Write(resource_name,
                              [(current_cycle, 20)]) == False, \
            'The Write call should fail because the resource does not exist.'
        
        success, data = self.API.Read(resource_name)
        assert success == False and data == None, \
            'The Read call should fail because the resource does not exist.'

        assert self.API.Create(resource_name, cycle) == True, \
            'The Create call should be successful.'
        
        expected_data = []
        success, data = self.API.Read(resource_name)
        assert success == True and data == expected_data, \
            'The Read call should be successful and data should match.'
        
        assert self.API.Write(resource_name, [(current_cycle, 20)]) == True, \
            'The Write call should be successful.'
        
        expected_data = [(current_cycle, 20)]
        success, data = self.API.Read(resource_name, current_cycle, next_cycle)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Should use current cycle value.')
        
        expected_data = [(current_cycle, 20)]
        success, data = self.API.Read(resource_name, current_cycle)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Should use current cycle value (implicity end_time).')
        
        expected_data = [(current_cycle, 20)]
        success, data = self.API.Read(resource_name)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Should use current cycle value (implicity start_time and '
             'end_time).')
        
        expected_data = []
        success, data = self.API.Read(resource_name, prev_cycle, current_cycle)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Should be empty because cycle is out of range.')
        
        expected_data = [(next_cycle, 20)]
        success, data = self.API.Read(resource_name, next_cycle)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Should give next cycle value (persisted from prev cycle).')
        
        expected_data = [(current_cycle, 20)]
        success, data = self.API.Read(resource_name,
                                      current_cycle + cycle // 2,
                                      next_cycle)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Should give current cycle, aligned to the correct timestamp.')
        
        expected_data = [(current_cycle, 20)]
        success, data = self.API.Read(resource_name,
                                      current_cycle + cycle // 2,
                                      next_cycle + cycle // 2)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Should give current cycle because although this spans two valid '
             'cycles, there is only one value written there, which is '
             'persisted for the future cycles.')
        
        assert self.API.Write(resource_name, [(next_cycle, 40)]) == True, \
            'The Write call should be successful.'
        
        expected_data = [(current_cycle, 20), (next_cycle, 40)]
        success, data = self.API.Read(resource_name,
                                      current_cycle + cycle // 2,
                                      next_cycle + cycle // 2)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Should give current and next cycle because the range spans two '
             'valid cycles with distinct entries.')
        
        expected_data = [(current_cycle, 20)]
        success, data = self.API.Read(resource_name,
                                      current_cycle + cycle // 2)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Should give only current cycle.')
        
        expected_data = [(current_cycle, 20)]
        success, data = self.API.Read(resource_name, next_cycle - 1)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Should give only current cycle, even if the read is at the '
             'boundary of a cycle.')
        
        expected_data = [(next_cycle, 40)]
        success, data = self.API.Read(resource_name, next_cycle)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'This should only give data from the next cycle, because it is '
             'the only valid cycle in the range.')
        
        expected_data = [(next_cycle + i*cycle, 10*i) for i in range(24)]

        assert self.API.Write(resource_name, expected_data) == True, \
            'The Write call for multiple entries should be successful.'  
        success, data = self.API.Read(resource_name,
                                      next_cycle,
                                      next_cycle + 24*cycle)
        assert success == True and data == expected_data, \
            ('The Read call should be successful and data should match. '
             'Reading the multiple written entries should work.')
        
    def test_api_copy_no_link(self):
        resource_orig = 'my_resource'
        cycle = 2 # should be 2 or more seconds here
        resource_copy = 'my_resource_copy'
        new_app_id = self.auth['app_id'] + 1
        current_cycle = int(ResourcePointer.GetNowSeconds())
        next_cycle = current_cycle + cycle

        assert self.API.Copy(resource_orig,
                             resource_copy,
                             is_link=False) == False, \
            ('The Copy call should be unsuccessful because the resource does '
             'not exist.')

        assert self.API.Create(resource_orig, cycle) == True, \
            'The Create call should be successful.'
        
        assert self.API.Copy(resource_orig,
                             resource_copy,
                             is_link=False) == True, \
            'The Copy call should be successful.'

        # Do chown to original then compare configs.
        new_app_access = {
            'id': new_app_id,
            'permission': Permission.NONE.name,
            'priority': Priority.LOW.name,
            'subscription': Subscription.NONE.name
        }
        assert self.API.Chown(resource_orig, new_app_id) == True, \
            'The Chown call should be successful.'
        success_orig, config_orig = self.API.GetConfig(resource_orig)
        success_copy, config_copy = self.API.GetConfig(resource_copy)
        assert success_orig == True and success_copy == True, \
            'The Config call for the original and copy should be successful.'
        assert config_orig['name'] != config_copy['name'], \
            'The names of the original and copy should not match.'
        assert config_orig['cycle_s'] == config_copy['cycle_s'], \
            'The cycles of the original and copy should match.'
        assert config_copy['is_link'] == False, \
            'The copy should not be a link because it is a data copy.'
        assert (len(config_orig['access_list']) != \
                    len(config_copy['access_list']) and
                new_app_access in config_orig['access_list'] and
                new_app_access not in config_copy['access_list']), \
            'The access lists of the original and copy should not match.'
        
        # Write to copy should not be available to read via copy and not via
        # original.
        expected_data_orig = []
        expected_data_copy = [(current_cycle, 20)]
        assert self.API.Write(resource_copy, expected_data_copy) == True, \
            'The Write call should be successful.'
        success_orig, data_orig = self.API.Read(resource_orig, current_cycle)
        success_copy, data_copy = self.API.Read(resource_copy, current_cycle)
        assert success_orig == True and success_copy == True, \
            'The Read call for the original and copy should be successful.'
        assert (data_orig != data_copy and
                data_orig == expected_data_orig and
                data_copy == expected_data_copy), \
            'The data read from the original and copy should not match.'
        
        # If the original resource is deleted, the copy still exists and is
        # functional.
        expected_data_copy = [(next_cycle, 40)]
        assert self.API.Destroy(resource_orig) == True, \
            'The Destroy call should be be successful.'
        assert self.API.Write(resource_copy, expected_data_copy) == True, \
            'The Write call should be successful.'
        success_copy, data_copy = self.API.Read(resource_copy, next_cycle)
        assert success_copy == True and data_copy == expected_data_copy, \
            ('The Read call should be successful and data should match. '
             'The copy should just be treated like an independent resource.')
    
    def test_api_copy_link(self):
        resource_orig = 'my_resource'
        cycle = 2 # should be 2 or more seconds here
        resource_copy = 'my_resource_copy'
        new_app_id = self.auth['app_id'] + 1
        current_cycle = int(ResourcePointer.GetNowSeconds())
        next_cycle = current_cycle + cycle

        assert self.API.Copy(resource_orig,
                             resource_copy,
                             is_link=True) == False, \
            ('The Copy call should be unsuccessful because the resource does '
             'not exist.')

        assert self.API.Create(resource_orig, cycle) == True, \
            'The Create call should be successful.'
        
        assert self.API.Copy(resource_orig,
                             resource_copy,
                             is_link=True) == True, \
            'The Copy call should be successful.'

        # Do chown to original then compare configs.
        new_app_access = {
            'id': new_app_id,
            'permission': Permission.NONE.name,
            'priority': Priority.LOW.name,
            'subscription': Subscription.NONE.name
        }
        assert self.API.Chown(resource_orig, new_app_id) == True, \
            'The Chown call should be successful.'
        success_orig, config_orig = self.API.GetConfig(resource_orig)
        success_copy, config_copy = self.API.GetConfig(resource_copy)
        assert success_orig == True and success_copy == True, \
            'The Config call for the original and copy should be successful.'
        assert config_orig['name'] != config_copy['name'], \
            'The names of the original and copy should not match.'
        assert config_orig['cycle_s'] == config_copy['cycle_s'], \
            'The cycles of the original and copy should match.'
        assert config_copy['is_link'] == True, \
            'The copy should be a link.'
        assert (len(config_orig['access_list']) != \
                    len(config_copy['access_list']) and
                new_app_access in config_orig['access_list'] and
                new_app_access not in config_copy['access_list']), \
            'The access lists of the original and copy should not match.'
        
        # Write to copy should be available to read via copy and also via
        # original.
        expected_data_copy = [(current_cycle, 20)]
        assert self.API.Write(resource_copy, expected_data_copy) == True, \
            'The Write call should be successful.'
        success_orig, data_orig = self.API.Read(resource_orig, current_cycle)
        success_copy, data_copy = self.API.Read(resource_copy, current_cycle)
        assert success_orig == True and success_copy == True, \
            'The Read call for the original and copy should be successful.'
        assert (data_orig == data_copy and data_orig == expected_data_copy), \
            'The data read from the original and copy should match.'
        
        # If the original resource is deleted, the copy still exists but is no
        # longer functional (broken link).
        expected_data_copy = [(next_cycle, 40)]
        assert self.API.Destroy(resource_orig) == True, \
            'The Destroy call should be be successful.'
        assert self.API.Write(resource_copy, expected_data_copy) == False, \
            ('The Write call should be unsuccessful because the resource no '
             'longer exists.')
        success_copy, data_copy = self.API.Read(resource_copy, next_cycle)
        assert success_copy == False, \
            ('The Read call should be unsuccessful because the resource no '
             'longer exists.')

    @pytest.mark.asyncio
    async def test_api_subscribe_none(self):
        resource_name = 'my_resource'
        cycle = 2 # should be 2 or more seconds here
        current_cycle = int(ResourcePointer.GetNowSeconds())
        next_cycle = current_cycle + cycle
        next_next_cycle = next_cycle + cycle
        handler = Mock()

        assert self.API.Create(resource_name, cycle) == True, \
            'The Create call should be successful.'
        
        assert self.API.Subscribe(resource_name, Subscription.NONE, handler)

        # Write to current, next, and later.
        data = [(current_cycle, 10), (next_cycle, 20), (next_next_cycle, 30)]
        assert self.API.Write(resource_name, data) == True, \
            'The Write call for multiple entries should be successful.'  

        # Assert no update.
        await asyncio.sleep(3)
        handler.assert_not_called()

    @pytest.mark.asyncio
    async def test_api_subscribe_current(self):
        resource_name = 'my_resource'
        cycle = 2 # should be 2 or more seconds here
        current_cycle = int(ResourcePointer.GetNowSeconds())
        next_cycle = current_cycle + cycle
        next_next_cycle = next_cycle + cycle
        handler = Mock()

        assert self.API.Create(resource_name, cycle) == True, \
            'The Create call should be successful.'
        
        assert self.API.Subscribe(resource_name, Subscription.CURRENT, handler)

        # Write to current, next, and later.
        data = [(current_cycle, 10), (next_cycle, 20), (next_next_cycle, 30)]
        assert self.API.Write(resource_name, data) == True, \
            'The Write call for multiple entries should be successful.'  

        # Assert update. Assert timestamp value is just current.
        await asyncio.sleep(3)
        full_name = Server.EXTERNAL_ADDRESS[0] + ':' + resource_name
        handler.assert_called_once_with(full_name, [current_cycle])

        # Switch this back to none such that it stops polling.
        assert self.API.Subscribe(resource_name, Subscription.NONE, handler)

    @pytest.mark.asyncio
    async def test_api_subscribe_next(self):
        resource_name = 'my_resource'
        cycle = 2 # should be 2 or more seconds here
        current_cycle = int(ResourcePointer.GetNowSeconds())
        next_cycle = current_cycle + cycle
        next_next_cycle = next_cycle + cycle
        handler = Mock()

        assert self.API.Create(resource_name, cycle) == True, \
            'The Create call should be successful.'
        
        assert self.API.Subscribe(resource_name, Subscription.NEXT, handler)

        # Write to current, next, and later.
        data = [(current_cycle, 10), (next_cycle, 20), (next_next_cycle, 30)]
        assert self.API.Write(resource_name, data) == True, \
            'The Write call for multiple entries should be successful.'  

        # Assert update. Assert timestamp value is just next.
        await asyncio.sleep(3)
        full_name = Server.EXTERNAL_ADDRESS[0] + ':' + resource_name
        handler.assert_called_once_with(full_name, [next_cycle])

        # Switch this back to none such that it stops polling.
        assert self.API.Subscribe(resource_name, Subscription.NONE, handler)

    @pytest.mark.asyncio
    async def test_api_subscribe_all(self):
        resource_name = 'my_resource'
        cycle = 2 # should be 2 or more seconds here
        current_cycle = int(ResourcePointer.GetNowSeconds())
        next_cycle = current_cycle + cycle
        next_next_cycle = next_cycle + cycle
        handler = Mock()

        assert self.API.Create(resource_name, cycle) == True, \
            'The Create call should be successful.'
        
        assert self.API.Subscribe(resource_name, Subscription.ALL, handler)

        # Write to current, next, and later.
        data = [(current_cycle, 10), (next_cycle, 20), (next_next_cycle, 30)]
        assert self.API.Write(resource_name, data) == True, \
            'The Write call for multiple entries should be successful.'  

        # Assert update. Assert timestamp value is for all.
        await asyncio.sleep(3)
        full_name = Server.EXTERNAL_ADDRESS[0] + ':' + resource_name
        handler.assert_called_once_with(full_name, [current_cycle,
                                                        next_cycle,
                                                        next_next_cycle])

        # Switch this back to none such that it stops polling.
        assert self.API.Subscribe(resource_name, Subscription.NONE, handler)
    

    
# TODO: Write Integration tests with multiple apps:
#       - Test Priority for writes.
#       - Test for multiple resource access (only owners can destroy or chown,
#       for example).