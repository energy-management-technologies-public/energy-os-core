import os
from eos.core.resource_pointer import ResourcePointer
import pytest

class TestResourcePointer:
    def setup_class(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        self.test_output_dir = os.path.join(dir_path, "output/")
        if not os.path.isdir(self.test_output_dir):
            os.mkdir(self.test_output_dir)
        ResourcePointer.SetRootDir(self.test_output_dir)
        self.resource_name = "my_resource"

    def remove_resourcefile(self):
        filename = ResourcePointer.GetFileName(self.resource_name)
        if os.path.exists(filename):
            os.remove(filename)

    def setup_method(self):
        self.remove_resourcefile()
    
    def teardown_method(self):
        self.remove_resourcefile()

    def test_rp_read_write_cycles(self):
        """
        Tests the basic read and write functionality of a Resource Pointer.
        """
        cycle = 10 # seconds
        rp = ResourcePointer.Create(self.resource_name, cycle, 1)
        assert rp.read() == [], "Read should be empty"

        now = int(ResourcePointer.GetNowSeconds())
        rp.write([(now, 20)])

        assert rp.read(now, now + cycle) == [(now, 20)], \
            "Should have the value we just wrote (using current cycle)"
        assert rp.read(now) == [(now, 20)], \
            "Implicitly setting the end should have the same effect"
        assert rp.read() == [(now, 20)], \
            "Implicitly setting the start and end should have the same effect"
        assert rp.read(now - cycle, now) == [], \
            "Should be empty because out of range in past"
        assert rp.read(now + cycle) == [(now + cycle, 20)], \
            ("Should use the value that was persisted, "
             "since none available for the current cycle.")
        assert rp.read(now + cycle//2,
                       now + cycle) == \
                [(now, 20)], \
            ("Should use the value that was persisted "
            "and aligned to the correct timestamp.")
        assert rp.read(now + cycle//2) == [(now, 20)], \
            ("This time spans two valid cycles. Should only give "
             "one because is just one persisted value.")
        
        rp.write([(now + cycle, 40)])
        assert rp.read(
            now + cycle//2, now + cycle + cycle // 2) == \
                [(now, 20), (now + cycle, 40)], \
            ("This time spans two valid cycles. Should give two aligned values "
             "because two distinct writes.")
        assert rp.read(now + cycle//2) == \
                [(now, 20)], \
            ("This should only give me one cycle's data.")
        assert rp.read(now + cycle - 1) == \
                [(now, 20)], \
            ("This should only give me one cycle's data, even when at the"
             "border.")
        assert rp.read(now + cycle) == \
                [(now + cycle, 40)], \
            ("This should only give me the next cycle's data.")
        return
    
    def test_rp_create_get(self):
        """
        Tests the functionality of creating and getting a Resource Pointer.
        """
        cycle = 10 # seconds
        rp = ResourcePointer.Create(self.resource_name, cycle, 1)
        now = int(ResourcePointer.GetNowSeconds())

        write_values = [(now, 10),
                        (now + cycle, 13),
                        (now + cycle*2, 16)
                        ]

        assert rp.read() == []
        rp.write(write_values)
        
        assert rp.read(now, now + cycle*3) == write_values

        with pytest.raises(FileExistsError) as e:
            ResourcePointer.Create(self.resource_name, cycle + 24845, 2)
        assert "already exists" in str(e.value)

        rp2 = ResourcePointer.LoadResearchPointer(self.resource_name)
        assert rp2.read(now, now + cycle*3) == write_values, \
            "Should be able to get current RP and have values be updated."
        
        # TODO: Add synchronization
        return
