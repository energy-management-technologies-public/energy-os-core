import os
import json
from eos.core.resource_pointer import ResourcePointer
from eos.core import API
from eos.core.common import *

if __name__ == '__main__':
    server_addr = str(input('internal server address: '))
    id = str(input('app id: '))
    auth_config_json = os.path.join(os.getcwd(), 'auth_config' + id + '.json')
    with open(auth_config_json) as f:
        auth_obj = json.load(f)
    auth_obj['server_addr'] = server_addr
    with open(auth_config_json, 'w') as f:
            json.dump(auth_obj, f)
    client_api = API(auth_config_json)
    print('API setup finished. You can use the api functions to interact with '
          'the server.')
    print('Note: all times and timestamps are in seconds.')
    print('You can use ResourcePointer.GetNowSeconds() as a utility, '
          'but will need to convert to an int().')
    print()
    print('The name of the variable is client_api. '
          'Try running \'help(client_api)\'.')